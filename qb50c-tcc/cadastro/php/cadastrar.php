<?php include("validar.php"); ?>
<?php include("cabecalho.php"); ?>

	<script type="text/javascript">

		function checkdate(input){
			var validformat=/^\d{2}\/\d{2}\/\d{4}$/ 
			var returnval=false
			if (!validformat.test(input.value))
				alert("Uma das duas datas não foi digitada corretamente. Observe o formato dd/mm/aaaa.")
			else {
				var dayfield=input.value.split("/")[0]
				var monthfield=input.value.split("/")[1]
				var yearfield=input.value.split("/")[2]
				var dayobj = new Date(yearfield, monthfield-1, dayfield)
					if ((dayobj.getDate()!=dayfield)||(dayobj.getMonth()+1!=monthfield)||(dayobj.getFullYear()!=yearfield))
						alert("Digite a data corretamente no formato dd/mm/aaaa.")
						else
						returnval=true
			}
			if (returnval==false) input.select()
			return returnval
			}
			
		function currencyFormat(fld, milSep, decSep, e) {
			var sep = 0;
			var key = '';
			var i = j = 0;
			var len = len2 = 0;
			var strCheck = '0123456789';
			var aux = aux2 = '';
			var whichCode = (window.Event) ? e.which : e.keyCode;
			if (whichCode == 13) return true; // Enter
			if (whichCode == 8) return true; // Delete
			key = String.fromCharCode(whichCode); // Get key value from key code
			if (strCheck.indexOf(key) == -1) return false; // Not a valid key
			len = fld.value.length;
			for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
			for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
				aux2 = '';
				for (j = 0, i = len - 3; i >= 0; i--) {
					if (j == 3) {
						aux2 += milSep;
						j = 0;
					}
					aux2 += aux.charAt(i);
					j++;
				}
				fld.value = '';
				len2 = aux2.length;
				for (i = len2 - 1; i >= 0; i--)
				fld.value += aux2.charAt(i);
				fld.value += decSep + aux.substr(len - 2, len);
			}
			return false;
		}
		
	</script>

</head>
<body>

<?php include("topo.php"); ?>

<div id="d_ntc_home2">
<div id="ntc_home">

<?php
if(file_exists("init.php")) {
	require "init.php";		
} else {
	echo "Erro: Arquivo init.php nao foi encontrado.";
	exit;
}

if(!function_exists("abre_conexao")) {
	echo "Erro: O arquivo init.php foi alterado, nao existe a função 'abre_conexao'.";
	exit;
}

abre_conexao();
$re = mysql_query("select * from catadores");
if(mysql_errno() != 0) {
	if(!isset($erros)) {
		echo "Erro: O arquivo init.php foi alterado, nao existe \$erros.";
		exit;
	}
	echo $erros[mysql_errno()];
	exit;
}
?>

<form action="salvar.php" name="formulario" id="formulario" onSubmit="return checkdate(this.data) && return checkdate(this.datanascimento)" method="post">

	<h2>Banco de Dados</h2>

	<p><label>Data <small>(dd/mm/aaa)</small></label> 
	<input type="text" size="10" name="data" id="data" />
		
	<label>Grupo</label> 
	<select name="grupo" id="grupo">
		<option></option>
		<option value="acamar">ACAMAR</option>
		<option value="acapra">Acapra</option>
		<option value="acaresti">ACARESTI</option>
		<option value="almirante_tamandare">Almirante Tamandaré</option>
		<option value="altonia">Altônia</option>
		<option value="amar">AMAR</option>
		<option value="amar_ebenezer">Amar Ebenezer</option>
		<option value="apava">APAVA</option>
		<option value="arafoz_coaafi">ARAFOZ/COAAFI</option>
		<option value="assama">ASSAMA</option>
		<option value="catamare">Catamare</option>
		<option value="ceram">Ceram</option>
		<option value="ceu_azul">Céu Azul</option>
		<option value="coopzumbi">Coopzumbi</option>
		<option value="diamante_doeste">Diamante D'Oeste</option>
		<option value="entre_rios_do_oeste">Entre Rios Do Oe</option>ste
		<option value="guaira">Guaíra</option>
		<option value="guaraniacu">Guaraniaçu</option>
		<option value="itaipulandia">Itaipulândia</option>
		<option value="jardim_icarai">Jardim Icaraí</option>
		<option value="marechal_candido_rondon">Marechal Cândido Rondon</option>
		<option value="maripa">Maripá</option>
		<option value="matelandia">Matelândia</option>
		<option value="mercedes">Mercedes</option>
		<option value="missal">Missal</option>
		<option value="moranguinho">Moranguinho</option>
		<option value="mundo_novo">Mundo Novo</option>
		<option value="nova_santa_rosa">Nova Santa Rosa</option>
		<option value="ouro_verde">Ouro Verde</option>
		<option value="pantanal">Pantanal</option>
		<option value="parolim">Parolim</option>
		<option value="pato_bragado">Pato Bragado</option>
		<option value="projeto_mutirao">Projeto Mutirão</option>
		<option value="quatro_pontes">Quatro Pontes</option>
		<option value="ramilandia">Ramilândia</option>
		<option value="reciclar">Reciclar</option>
		<option value="recilapa">Recilapa</option>
		<option value="resol">Resol</option>
		<option value="rio_negro">Rio Negro</option>
		<option value="santa_helena">Santa Helena</option>
		<option value="santa_tereza_do_oeste">Santa Tereza do Oeste</option>
		<option value="santo_anibal">Santo Anibal</option>
		<option value="sao_jose_das_palmeiras">São José Das Palmeiras</option>
		<option value="sao_pedro_do_igaucu">São Pedro do Iguaçú</option>
		<option value="savana">Savana</option>
		<option value="sociedade_unidade">Sociedade Unidade</option>
		<option value="terra_roxa">Terra Roxa</option>
		<option value="toledo">Toledo</option>
		<option value="vera_cruz_do_oeste">Vera Cruz do Oeste</option>
		<option value="vila_santa_maria">Vila Santa Maria</option>
	</select></p>

	<h2>Dados Pessoais</h2>

	<p><label>Nome Completo</label> <input type="text" size="40" name="nome" id="nome" value="" /></p>

	<p><label>Endereço</label> <input type="text" size="50" name="endereco" id="endereco" value="" /></p>
	
	<p>Mora em Curitiba, no Bairro: 
	<select name="bairro" id="bairro" type="varchar">
		<option></option>
		<option value="abranches">Abranches</option>
		<option value="agua_verde">Água Verde</option>
		<option value="ahu">Ahú</option>
		<option value="alto_boqueirao">Alto Boqueirão</option>
		<option value="alto_da_gloria">Alto da Glória</option>
		<option value="alto_da_rua_xv">Alto da Rua XV</option>
		<option value="atuba">Atuba</option>
		<option value="augusta">Augusta</option>
		<option value="bacacheri">Bacacheri</option>
		<option value="bairro_alto">Bairro Alto</option>
		<option value="barreirinha">Barreirinha</option>
		<option value="batel">Batel</option>
		<option value="bigorrilho">Bigorrilho</option>
		<option value="boa_vista">Boa Vista</option>
		<option value="bom_retiro">Bom Retiro</option>
		<option value="boqueirao">Boqueirão</option>
		<option value="butiatuvinha">Butiatuvinha</option>
		<option value="cabral">Cabral</option>
		<option value="cachoeira">Cachoeira</option>
		<option value="cajuru">Cajuru</option>
		<option value="campina_grande">Campina do Siqueira</option>
		<option value="campo_comprido">Campo Comprido</option>
		<option value="campo_de_santana">Campo de Santana</option>
		<option value="capao_da_imbuia">Capão da Imbuia</option>
		<option value="capao_raso">Capão Raso</option>
		<option value="cascatinha">Cascatinha</option>
		<option value="caximba">Caximba</option>
		<option value="centro">Centro</option>
		<option value="centro_civico">Centro Cívico</option>
		<option value="cidade_industrial_de_curitiba">Cidade Industrial de Curitiba</option>
		<option value="cristo_rei">Cristo Rei</option>
		<option value="fanny">Fanny</option>
		<option value="fazendinha">Fazendinha</option>
		<option value="ganchinho">Ganchinho</option>
		<option value="guabirotuba">Guabirotuba</option>
		<option value="guaira">Guaíra</option>
		<option value="hauer">Hauer</option>
		<option value="hugo_lange">Hugo Lange</option>
		<option value="jardim_botanico">Jardim Botânico</option>
		<option value="jardim_das_americas">Jardim das Américas</option>
		<option value="jardim_social">Jardim Social</option>
		<option value="juveve">Juvevê</option>
		<option value="lamenha_pequena">Lamenha Pequena</option>
		<option value="lindoia">Lindóia</option>
		<option value="merces">Mercês</option>
		<option value="mossungue">Mossunguê</option>
		<option value="novo_mundo">Novo Mundo</option>
		<option value="orleans">Orleans</option>
		<option value="parolin">Parolin</option>
		<option value="pilarzinho">Pilarzinho</option>
		<option value="pinheirinho">Pinheirinho</option>
		<option value="portao">Portão</option>
		<option value="prado_velho">Prado Velho</option>
		<option value="reboucas">Rebouças</option>
		<option value="riviera">Riviera</option>
		<option value="santa_candida">Santa Cândida</option>
		<option value="santa_felicidade">Santa Felicidade</option>
		<option value="santa_quiteria">Santa Quitéria</option>
		<option value="santo_inacio">Santo Inácio</option>
		<option value="sao_braz">São Braz</option>
		<option value="sao_francisco">São Francisco</option>
		<option value="sao_joao">São João</option>
		<option value="sao_lourenco">São Lourenço</option>
		<option value="sao_miguel">São Miguel</option>
		<option value="seminario">Seminário</option>
		<option value="sitio_cercado">Sítio Cercado</option>
		<option value="taboao">Taboão</option>
		<option value="taruma">Tarumã</option>
		<option value="tatuquara">Tatuquara</option>
		<option value="tingui">Tingüi</option>
		<option value="uberaba">Uberaba</option>
		<option value="umbara">Umbará</option>
		<option value="vila_izabel">Vila Izabel</option>
		<option value="vista_alegre">Vista Alegre</option>
		<option value="xaxim">Xaxim</option></select></p>

	<p>Mora na Região Metropolitana: 
	<select name="regiaometropolitana" id="regiaometropolitana" type="varchar">
		<option></option>
		<option value="adrianopolis">Adrianópolis</option>
		<option value="agudos_do_sul">Agudos do Sul</option>
		<option value="almirante_tamandare">Almirante Tamandaré</option>
		<option value="araucaria">Araucária</option>
		<option value="balsa_nova">Balsa Nova</option>
		<option value="bocaiuva_do_sul">Bocaiúva do Sul</option>
		<option value="campina_grande_do_sul">Campina Grande do Sul</option>
		<option value="campo_largo">Campo Largo</option>
		<option value="campo_magro">Campo Magro</option>
		<option value="cerro_azul">Cerro Azul</option>
		<option value="colombo">Colombo</option>
		<option value="contenda">Contenda</option>
		<option value="doutor_ulysses">Doutor Ulysses</option>
		<option value="fazenda_rio_grande">Fazenda Rio Grande</option>
		<option value="itaperucu">Itaperuçu</option>
		<option value="lapa">Lapa</option>
		<option value="mandirituba">Mandirituba</option>
		<option value="pinhais">Pinhais</option>
		<option value="piraquara">Piraquara</option>
		<option value="quatro_barras">Quatro Barras</option>
		<option value="quitandinha">Quitandinha</option>
		<option value="rio_branco_do_sul">Rio Branco do Sul</option>
		<option value="sao_jose_dos_pinhais">São José dos Pinhais</option>
		<option value="tijucas_do_sul">Tijucas do Sul</option>
		<option value="tunas_do_parana">Tunas do Paraná</option></select></p>

	<p><label >Telefone</label> <input type="text" size="20" name="telefone" id="telefone" /></p>
	
	<p><label>Data de Nascimento <small>(dd/mm/aaa)</small></label> <input type="text" size="10" name="datanascimento" id="datanascimento" /></p> 

	<p><label>Sexo</label><br/>
	<input type="radio" name="sexo" id="sexo" value="masculino" /><label>Masculino</label>&#09;
	<input type="radio" name="sexo" id="sexo" value="feminino" /><label>Feminino</label></p>

	<p><label>Raça</label><br />
		<input type="radio" name="raca" id="raca" value="branca" /><label>Branca</label>&#09; 
		<input type="radio" name="raca" id="raca" value="negra" /><label>Negra</label>&#09;
		<input type="radio" name="raca" id="raca" value="parda" /><label>Parda</label>&#09;
		<input type="radio" name="raca" id="raca" value="amarela" /><label>Amarela</label>&#09;
		<input type="radio" name="raca" id="raca" value="indigena" /><label>Indígena</label></p>

	<p><label>Documentos</label><br />
		<input type="checkbox" name="certidao_de_nascimento" id="certidao_de_nascimento" value="certidao_de_nascimento" /><label>Certidão de Nascimento</label> <br />
		<input type="checkbox" name="certidao_de_casamento" id="certidao_de_casamento" value="certidao_de_casamento" /><label>Certidão de Casamento</label> <br /> 
		<input type="checkbox" name="rg" id="rg" value="rg" /><label>RG</label> <br /> 
		<input type="checkbox" name="cpf" id="cpf" value="cpf" /><label>CPF</label> <br /> 
		<input type="checkbox" name="titulo_de_eleitor" id="titulo_de_eleitor" value="titulo_de_eleitor" /><label>Título de Eleitor</label> <br /> 
		<input type="checkbox" name="carteira_de_trabalho" id="carteira_de_trabalho" value="carteira_de_trabalho" /><label>Carteira de Trabalho</label> <br /> 
		<input type="checkbox" name="pis" id="pis" value="pis" /><label>PIS</label></p>

	<p><label>Nascido em:</label>
		<input type="radio" name="nascido_em" id="nascido_em" value="cidade" /><label>Cidade</label>&#09;
		<input type="radio" name="nascido_em" id="nascido_em" value="campo" /><label>Campo</label><br />
		<label>Nome da Cidade:</label> <input type="text" size="20" name="cidade_natal" id="cidade_natal" value="" /></p>
		
		
	<p><label>Desde Que Ano Reside Nesta Cidade <small>(aaaa)</small></label> <input type="text" size="5" name="tempo_de_residencia" value="" id="tempo_de_residencia" /></p>

	<h2>Família</h2>
	
	<p><input type="checkbox" name="chefe_de_familia" id="chefe_de_familia" value="chefe_de_familia" />&#09;
  <label>É Chefe de Família</label></p>
  
	<p><label >Número de Filhos</label> <input type="text" size="5" name="filhos" value="" id="filhos" /></p>

	<p><label>Estado Civil</label><br />
	<input type="radio" name="estado_civil" id="estado_civil" value="solteiro" /> <label>Solteiro(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="casado" /><label>Casado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="amigado" /><label>Amigado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="separado" /><label>Separado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="viuvo" /> <label>Viúvo(a)</label>&#09;</p>

	<p>Número de Pessoas na Família<br />
	<label>Que Residem em Conjunto</label> <input type="text" size="5" name="residem_em_conjunto" value="" id="residem_em_conjunto" /><br />
	<label>Menores de 12 anos</label> <input type="text" size="5" name="menores_de_12" value="" id="menores_de_12" /><br />
	<label>Entre 12 e 18 anos</label> <input type="text" size="5" name="entre_12_e_18" value="" id="entre_12_e_18" /><br />
	<label>Que Trabalham</label> <input type="text" size="5" name="que_trabalham" value="" id="que_trabalham" /><br /></p>

	<h2>Moradia</h2>

	<p><label>Classificação da Residência</label><br />
		<input type="radio" name="residencia" id="residencia" value="casa" /><label>Casa</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="comodo" /><label>Cômodo</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="pensao" /><label>Pensão</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="lixao" /><label>Lixão</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="deposito" /><label>Depósito</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="albergue" /><label>Albergue</label>&#09;
		<input type="radio" name="residencia" id="residencia" value="rua" /> <label>Rua</label><br />
		<label>Outro</label> <input type="text" size="10" name="residencia_outro" value="" id="residencia_outro" /></p>

	<p><label>Situação da Residência</label><br />
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="propria_sem_documentos" /><label>Própria Com Documentos</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="propria_com_documentos" /><label>Própria Sem Documentos</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="alugada" /><label>Alugada</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="cedida" /><label>Cedida</label><br />
	<label>Outro</label> <input type="text" size="10" name="situacao_da_residencia_outro" value="" id="situacao_da_residencia_outro" /></p>
	
	<p><label>Tipo de Construção</label><br />
	<input type="radio" name="tipo_de_construcao" id="tipo_de_construcao" value="alvenaria" /><label>Alvenaria</label>&#09;
	<input type="radio" name="tipo_de_construcao" id="tipo_de_construcao" value="madeira" /><label>Madeira</label><br />
	<label>Outro</label> <input type="text" size="10" name="tipo_de_construcao_outro" id="tipo_de_construcao_outro" value="" /></p>
	
	<h2>Renda</h2>	
	
	<p><input type="checkbox" name="contribuinte_do_inss" id="contribuinte_do_inss" value="contribuinte_do_inss" /> <label>Contribuinte do INSS</label></p>
	
	<p>Não contribui ao INSS por:<br />
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="renda" /><label>Falta de Renda</label>&#09;
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="informacoes" /><label>Falta de Informações</label>&#09;
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="nao_quer" /><label>Não Querer Contribuir</label><br />
	<label>Outro</label> <input type="text" size="10" name="nao_contribui_ao_inss_outro" id="nao_contribui_ao_inss_outro" value="" /></p>


	<p><label>Renda Familiar Aproximada</label>&#09; R$<input type="text" size="10" name="renda_familiar" value="" id="renda_familiar" onKeyPress="return(currencyFormat(this,',','.',event))"></p>
	
	<p><label>Remuneração</label><br />
	<input type="radio" name="remuneracao" id="remuneracao" value="150" /><label>Até R$150,00</label><br />
	<input type="radio" name="remuneracao" id="remuneracao" value="151_e_300" /><label>Entre R$151,00 e R$300,00</label> <br />
	<input type="radio" name="remuneracao" id="remuneracao" value="301_e_450" /><label>Entre R$301,00 e R$450,00</label> <br /> 
	<input type="radio" name="remuneracao" id="remuneracao" value="451_e_600" /><label>Entre R$451,00 e R$600,00</label> <br /> 
	<input type="radio" name="remuneracao" id="remuneracao" value="600" /><label>Acima de R$600,00</label></p>

	<p><label>Água Encanada</label><br />
	<input type="radio" name="agua_encanada" id="agua_encanada" value="sanepar" /><label>Sanepar</label>&#09;
	<input type="radio" name="agua_encanada" id="agua_encanada" value="informal" /><label>Informal</label>&#09;
	<input type="radio" name="agua_encanada" id="agua_encanada" value="nao_possui" /><label>Não Possui</label></p>

	<p><label>Energia Elétrica</label><br />
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="copel" /><label>Copel</label>&#09;
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="informal" /><label>Informal</label>&#09;
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="nao_possui" /><label>Não Possui</label></p>

	<p><label>Esgoto</label><br />
	<input type="radio" name="esgoto" id="esgoto" value="rede_publica" /><label>Rede Pública</label>&#09;
	<input type="radio" name="esgoto" id="esgoto" value="fossa" /><label>Fossa</label>&#09;
	<input type="radio" name="esgoto" id="esgoto" value="ceu_aberto" /><label>Céu Aberto</label></p>

	<p><input type="checkbox" name="possui_banheiro" id="possui_banheiro" value="possui_banheiro" /><label>Possui Banheiro</label></p>
	
	<h2>Estudos</h2>
	
	<p><label>Estudou Até a</label>
	<select name="estudou_ate" id="estudou_ate" type="varchar">
		<option></option>
		<option value="1a_serie">1&ordf; Série</option>
		<option value="2a_serie">2&ordf; Série</option>
		<option value="3a_serie">3&ordf; Série</option>
		<option value="4a_serie">4&ordf; Série</option>
		<option value="5a_serie">5&ordf; Série</option>
		<option value="6a_serie">6&ordf; Série</option>
		<option value="7a_serie">7&ordf; Série</option>
		<option value="8a_serie">8&ordf; Série</option>
		<option value="1a_serie_2o_grau">1&ordf; Série do Segundo Grau</option>
		<option value="2a_serie_2o_grau">2&ordf; Série do Segundo Grau</option>
		<option value="3a_serie_2o_grau">3&ordf; Série do Segundo Grau</option>
	</select></p>

	<p><label>Gostaria de Estudar</label>
	<input type="checkbox" name="supletivo" id="supletivo" value="supletivo" /><label>Supletivo</label>&#09;
	<input type="checkbox" name="informatica" id="informatica" value="informatica" /><label>Informática</label>&#09;
	<input type="checkbox" name="reciclagem" id="reciclagem" value="reciclagem" /><label>Reciclagem</label>&#09;
	<input type="checkbox" name="alfabetizacao" id="alfabetizacao" value="alfabetizacao" /><label>Alfabetização</label>&#09;
	<input type="checkbox" name="gestao" id="gestao" value="gestao" /><label>Gestão de Empreendimentos</label>&#09;
	<input type="checkbox" name="artesanato" id="artesanato" value="artesanato" /><label>Artesanato</label><br />
	<label>Outro</label> <input type="text" size="20" name="estudar_outro" id="estudar_outro" value="" /></p>

	<h2>Saúde</h2>

	<p><input type="checkbox" name="fuma" id="fuma" value="fuma" /><label>Fuma</label>&#09;
	<input type="checkbox" name="bebe" id="bebe" value="bebe" /><label>Bebe</label>&#09;
	<input type="checkbox" name="drogas" id="drogas" value="drogas" /><label>Usa Drogas</label><br /></p>
	
	<p>Parou de fumar no ano de <small>(aaaa)</small> <input type="text" size="5" name="fumava" id="fumava" value="" /></p>

	<p>Parou de beber no ano de <small>(aaaa)</small> <input type="text" size="5" name="bebia" id="bebia" value="" /></p>

	<p>Parou de usar drogas no ano de <small>(aaaa)</small> <input type="text" size="5" name="usava_drogas" id="usava_drogas" value="" /></p>

	<p><input type="checkbox" name="frequenta_medico" id="frequenta_medico" value="frequenta_medico" /><label>Freqüenta Médico Regularmente</label><br />
	<input type="checkbox" name="faz_tratamento_de_saude" id="faz_tratamento_de_saude" value="faz_tratamento_de_saude" /><label>Faz Tratamento de Saúde</label><br />
	<input type="checkbox" name="toma_medicamentos" id="toma_medicamentos" value="toma_medicamentos" /><label>Toma Medicamentos Regularmente</label><br />	
	<input type="checkbox" name="faz_acompanhamento_psicologico" id="faz_acompanhamento_psicologico" value="faz_acompanhamento_psicologico" /><label>Faz Acompanhamento Psicológico</label><br />	
	<input type="checkbox" name="frequenta_dentista" id="frequenta_dentista" value="frequenta_dentista" /><label>Freqüenta Dentista Regularmente</label></p>
	
	<p><label>Como é o atendimento do Posto de Saúde da Sua Região?</label><br />
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="otimo" /><label>Ótimo</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="bom" /> <label>Bom</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="regular" /><label>Regular</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="ruim" /><label>Ruim</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="pessimo" /><label>Péssimo</label> <br />

	<p><input type="checkbox" name="conhece_psf" id="conhece_psf" value="conhece_psf" /><label>Conhece o Programa Saúde da Família em Sua Região</label><br />
	<input type="checkbox" name="ja_foi_atendido_psf" id="ja_foi_atendido_psf" value="ja_foi_atendido_psf" /><label>Já Foi Atendido Pelo Programa Saúde da Família</label></p>

	<p><input type="checkbox" name="conhece_conselho_saude_local" id="conhece_conselho_saude_local" value="conhece_conselho_saude_local" /><label>Conhece o Conselho de Saúde Local</label><br />
	<input type="checkbox" name="participou_conselho_saude_local" id="participou_conselho_saude_local" value="participou_conselho_saude_local" /><label>Já Participou do Conselho de Saúde Local</label></p>

	<h2>Trabalho</h2>
	
	<p><label>Religião</label>
	<select name="religiao" id="religiao" type="varchar">
		<option></option>
		<option value="evangelica">Evangélica</option>
		<option value="catolica">Católica</option>
	</select></p>
	
	<p><label>Trabalhou em Outra Coisa Antes de Ser Catador:</label> <input type="text" size="15" name="trabalhou_antes" value="" id="trabalhou_antes" /></p>
	
	<p><input type="checkbox" name="trabalhou_carteira_assinada" id="trabalhou_carteira_assinada" value="trabalhou_carteira_assinada" /> <label>Já Trabalhou de Carteira Assinada</label></p>

	<p><label>É Catador Desde o Ano de <small>(aaaa)</small></label> <input type="text" size="5" name="tempo_que_e_catador" value="" id="tempo_que_e_catador" /></p>

	<p><input type="checkbox" name="sofreu_acidente" id="sofreu_acidente" value="sofreu_acidente" /> <label>Já Sofreu Acidente de Trânsito Como Catador</label></p>
	
	<p><label>Tem Outro Emprego Além de Catador:</label> <input type="text" size="15" name="outro_emprego" value="" id="outro_emprego" /></p>

	<p>Benefícios Pessoais Recebidos:<br />
	<input type="checkbox" name="pensao" id="pensao" value="pensao" /><label>Pensão</label>&#09;
	<input type="checkbox" name="aposentadoria" id="aposentadoria" value="aposentadoria" /><label>Aposentadoria</label>&#09;
	<input type="checkbox" name="seguro_desemprego" id="seguro_desemprego" value="seguro_desemprego" /><label>Seguro Desemprego</label>&#09;
	<input type="checkbox" name="auxilio_doenca" id="auxilio_doenca" value="auxilio_doenca" /><label>Auxílio Doença</label>&#09;
	<input type="checkbox" name="beneficio_de_prestacao_continuada" id="beneficio_de_prestacao_continuada" value="beneficio_de_prestacao_continuada" /><label>Benefício de Prestação Continuada</label><br />
	<label>Outro</label> <input type="text" size="10" name="beneficio_outro" value="" id="beneficio_outro" /></p>

	<p>Benefícios Familiares Recebidos:<br />
	<input type="checkbox" name="pensao_familia" id="pensao_familia" value="pensao_familia" /><label>Pensão</label>&#09;
	<input type="checkbox" name="aposentadoria_familia" id="aposentadoria_familia" value="aposentadoria_familia" /><label>Aposentadoria</label>&#09;
	<input type="checkbox" name="seguro_desemprego_familia" id="seguro_desemprego_familia" value="seguro_desemprego_familia" /><label>Seguro Desemprego</label>&#09;
	<input type="checkbox" name="auxilio_doenca_familia" id="auxilio_doenca_familia" value="auxilio_doenca_familia" /><label>Auxílio Doença</label>&#09;
	<input type="checkbox" name="beneficio_de_prestacao_continuada_familia" id="beneficio_de_prestacao_continuada_familia" value="beneficio_de_prestacao_continuada_familia" /><label>Benefício de Prestação Continuada</label><br />
	<label>Outro</label> <input type="text" size="10" name="beneficio_familia_outro" value="" id="beneficio_familia_outro" /></p>
	
	<p>Programas Sociais em Que a Família Está Envolvida:<br />
	<input type="checkbox" name="peti" id="peti" value="peti" /><label>PETI</label>&#09;
	<input type="checkbox" name="bolsa_familia" id="bolsa_familia" value="bolsa_familia" /><label>Bolsa Família</label>&#09;
	<input type="checkbox" name="programa_do_leite" id="programa_do_leite" value="programa_do_leite" /><label>Programa do leite</label>&#09;
	<input type="checkbox" name="luz_fraterna" id="luz_fraterna" value="luz_fraterna" /><label>Luz Fraterna</label>&#09;
	<input type="checkbox" name="tarifa_social_da_agua" id="tarifa_social_da_agua" value="tarifa_social_da_agua" /><label>Tarifa Social da Água</label>&#09;
	<input type="checkbox" name="agente_jovem" id="agente_jovem" value="agente_jovem" /><label>Agente Jovem</label><br />
	<label>Outro</label> <input type="text" size="10" name="programas_sociais_outro" value="" id="programas_sociais_outro" /><br />
	<label>Doações</label> <input type="text" size="10" name="doacoes" value="" id="doacoes" /></p>

	<p>Distância do grupo, associação ou cooperativa <small>(em metros)</small><input type="text" size="5" name="distancia" value="" id="distancia" /></p>

	<p><label>Como normalmente se locomove ao trabalho:</label><br />
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="a_pe" /><label>A Pé</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="puxando_carrinho" /> <label>Puxando o Carrinho</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="onibus" /><label>Ônibus</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="carro" /><label>Carro</label><br />
	<label>Outro</label> <input type="text" size="10" name="locomocao_ao_trabalho_outro" value="" id="locomocao_ao_trabalho_outro" /></p>

	<h2>Organização Trabalhista</h2>
	
	<p><label>Ocupa Algum Cargo na Diretoria da Associação? Qual?</label> <input type="text" size="10" name="cargo" value="" id="cargo" /></p>
	
	<p>Conhece as Seguintes Orgaizações:<br />
	<input type="checkbox" name="mncr" id="mncr" value="mncr" /><label>Movimento Nacional de Catadores de Material Reciclável</label><br />
	<input type="checkbox" name="instituto_lixo_cidadania" id="instituto_lixo_cidadania" value="instituto_lixo_cidadania" /><label>Instituto Lixo e Cidadania</label><br />
	<input type="checkbox" name="outras_associacoes" id="outras_associacoes" value="outras_associacoes" /><label>Outras Associações de Catadores</label><br />
	<label>Outro</label> <input type="text" size="10" name="organizacoes_outro" value="" id="organizacoes_outro" /></p>

	<p><label>Já Participou de Algum Encontro de Catadores? Qual? </label> 
	<input type="text" size="25" name="participou_de_encontro" value="" id="participou_de_encontro" /></p>
	
	<p><label>Condições de Trabalho Após Filiação com a Associação</label><br />
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="melhores" /><label>Melhores</label>&#09;
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="piores" /><label>Piores</label>&#09;	
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="nao_mudaram" /><label>Não Mudaram</label><br />
	<label>Porquê?</label> <input type="text" size="20" name="condicoes_apos_filiacao_porque" value="" id="condicoes_apos_filiacao_porque" /></p>
	
	<p><input type="checkbox" name="aumento_de_rendimentos" id="aumento_de_rendimentos" value="aumento_de_rendimentos" /><label>Teve Aumento de Rendimentos Após Filiação</label></p>
	
	<p>Vantagens da Filiação<br />
	<input type="checkbox" name="vantagem_remuneracao" id="vantagem_remuneracao" value="vantagem_remuneracao" /><label>Remuneração</label>&#09;
	<input type="checkbox" name="vantagem_equipamentos_de_trabalho" id="vantagem_equipamentos_de_trabalho" value="vantagem_equipamentos_de_trabalho" /><label>Equipamentos de Trabalho</label>&#09;
	<input type="checkbox" name="vantagem_carga_horaria" id="vantagem_carga_horaria" value="vantagem_carga_horaria" /><label>Carga Horária</label>&#09;
	<input type="checkbox" name="vantagem_cursos" id="vantagem_cursos" value="vantagem_cursos" /><label>Cursos</label>&#09;
	<input type="checkbox" name="vantagem_doacoes" id="vantagem_doacoes" value="vantagem_doacoes" /><label>Doações</label>&#09;
	<input type="checkbox" name="vantagem_organizacao_da_comunidade" id="vantagem_organizacao_da_comunidade" value="vantagem_organizacao_da_comunidade" /><label>Organização da Comunidade</label>&#09;
	<input type="checkbox" name="vantagem_participacao_da_familia" id="vantagem_participacao_da_familia" value="vantagem_participacao_da_familia" /><label>Participação da Família</label>&#09;
	<input type="checkbox" name="vantagem_contato_com_instituicoes_de_apoio" id="vantagem_contato_com_instituicoes_de_apoio" value="vantagem_contato_com_instituicoes_de_apoio" /><label>Contato com Instituições de Apoio</label></p>
	
	<p><input type="checkbox" name="preco_de_venda" id="preco_de_venda" value="preco_de_venda" /><label>Sabe o Preço de Venda dos Recicláveis</label></p>
	
	<p>Quem Mais Na Família é Catador?<br />
	<input type="checkbox" name="catador_pais" id="catador_pais" value="catador_pais" /><label>Pais</label>&#09;
	<input type="checkbox" name="catador_irmaos" id="catador_irmaos" value="catador_irmaos" /><label>Irmãos</label>&#09;
	<input type="checkbox" name="catador_esposo" id="catador_esposo" value="catador_esposo" /><label>Esposo(a)</label>&#09;
	<input type="checkbox" name="catador_filhos" id="catador_filhos" value="catador_filhos" /><label>Filhos</label><br />
	<label>Outros</label> <input type="text" size="15" name="catador_outro" value="" id="catador_outro" /></p>
	
	<p><label>Como Considera a Condição Familiar</label><br />
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="otima" /><label>Ótima</label>&#09;
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="boa" /><label>Boa</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="regular" /><label>Regular</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="ruim" /><label>Ruim</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="pessima" /><label>Péssima</label></p>

	<p>Maiores Dificuldades Encontradas no Trabalho<br />
	<input type="checkbox" name="falta_de_organizacao" id="falta_de_organizacao" value="falta_de_organizacao" /><label>Falta de Organização</label>&#09;
	<input type="checkbox" name="distancias" id="distancias" value="distancias" /><label>Distâncias Para Trabalhar</label>&#09;
	<input type="checkbox" name="inseguranca_financeira" id="inseguranca_financeira" value="inseguranca_financeira" /><label>Insegurança Financeira</label>&#09;
	<input type="checkbox" name="baixa_remuneracao" id="baixa_remuneracao" value="baixa_remuneracao" /><label>Baixa Remuneração</label>&#09;
	<input type="checkbox" name="relacionamento_catadores" id="relacionamento_catadores" value="relacionamento_catadores" /><label>Relacionamento com Outros Catadores</label>&#09;
	<input type="checkbox" name="relacionamento_comunidade" id="relacionamento_comunidade" value="relacionamento_comunidade" /><label>Relacionamento com a Comunidade</label>&#09;
	<input type="checkbox" name="falta_de_equipamentos" id="falta_de_equipamentos" value="falta_de_equipamentos" /><label>Falta de Equipamentos</label></p>
	
	<p>Quem Considera Líderes do Grupo<br />
	<label>Líder 1:</label> <input type="text" size="25" name="lider_1" value="" id="lider_1" /><br />
	<label>Líder 2:</label> <input type="text" size="25" name="lider_2" value="" id="lider_2" /></p>
	
	<input type="submit" value="Salvar" />
</form>
<?php include("rodape.php"); ?>
