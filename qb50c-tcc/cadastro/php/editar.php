<?php include("validar.php"); ?>
<?php include("cabecalho.php"); ?>
</head>
<body>

<?php include("topo.php"); ?>

<div id="d_ntc_home2">
<div id="ntc_home">

	<script type="text/javascript">

		function checkdate(input){
			var validformat=/^\d{2}\/\d{2}\/\d{4}$/ 
			var returnval=false
			if (!validformat.test(input.value))
				alert("Uma das duas datas não foi digitada corretamente. Observe o formato dd/mm/aaaa.")
			else {
				var dayfield=input.value.split("/")[0]
				var monthfield=input.value.split("/")[1]
				var yearfield=input.value.split("/")[2]
				var dayobj = new Date(yearfield, monthfield-1, dayfield)
					if ((dayobj.getDate()!=dayfield)||(dayobj.getMonth()+1!=monthfield)||(dayobj.getFullYear()!=yearfield))
						alert("Digite a data corretamente no formato dd/mm/aaaa.")
						else
						returnval=true
			}
			if (returnval==false) input.select()
			return returnval
			}
			
		function currencyFormat(fld, milSep, decSep, e) {
			var sep = 0;
			var key = '';
			var i = j = 0;
			var len = len2 = 0;
			var strCheck = '0123456789';
			var aux = aux2 = '';
			var whichCode = (window.Event) ? e.which : e.keyCode;
			if (whichCode == 13) return true; // Enter
			if (whichCode == 8) return true; // Delete
			key = String.fromCharCode(whichCode); // Get key value from key code
			if (strCheck.indexOf(key) == -1) return false; // Not a valid key
			len = fld.value.length;
			for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
			for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
				aux2 = '';
				for (j = 0, i = len - 3; i >= 0; i--) {
					if (j == 3) {
						aux2 += milSep;
						j = 0;
					}
					aux2 += aux.charAt(i);
					j++;
				}
				fld.value = '';
				len2 = aux2.length;
				for (i = len2 - 1; i >= 0; i--)
				fld.value += aux2.charAt(i);
				fld.value += decSep + aux.substr(len - 2, len);
			}
			return false;
		}
		
	</script>

</head>
<body>

<?php
if(file_exists("init.php")) {
	require "init.php";		
} else {
	echo "Erro: O Arquivo init.php nao foi encontrado.";
	exit;
}

if(!function_exists("abre_conexao")) {
	echo "Erro: O arquivo init.php foi alterado, nao existe a função 'abre_conexao'.";
	exit;
}
$id = $_GET["id"];

abre_conexao();
$re    = mysql_query("select count(*) as total from catadores where id = $id");	
$total = mysql_result($re, 0, "total");

if($total == 1) {
	$re    = mysql_query("select * from catadores where id = $id");
	$dados = mysql_fetch_array($re);		
}
?>

<form action="salvar-editar.php?id=<?php echo $id; ?>" name="formulario" id="formulario" onSubmit="return checkdate(this.data) && return checkdate(this.datanascimento)" method="post">

	<h2>Banco de Dados</h2>

	<p><label>Data <small>(dd/mm/aaa)</small></label> 
	<input type="text" size="10" name="data" id="data" value="<?php echo $dados['data']; ?>" />

	<label>Grupo</label> 
	<select name="grupo" id="grupo">
		<option></option>
		<option value="acamar" <?php if ($dados["grupo"] == 'acamar') echo 'selected="selected"';?>>ACAMAR</option>
		<option value="ceu_azul" <?php if ($dados["grupo"] == 'ceu_azul') echo 'selected="selected"';?>>Céu Azul</option>
		<option value="diamante_doeste" <?php if ($dados["grupo"] == 'diamante_doeste') echo 'selected="selected"';?>>Diamante D'Oeste</option>
		<option value="entre_rios_do_oeste" <?php if ($dados["grupo"] == 'entre_rios_do_oeste') echo 'selected="selected"';?>>Entre Rios Do Oe</option>ste
		<option value="arafoz_coaafi" <?php if ($dados["grupo"] == 'arafoz_coaafi') echo 'selected="selected"';?>>ARAFOZ/COAAFI</option>
		<option value="guaira" <?php if ($dados["grupo"] == 'guaira') echo 'selected="selected"';?>>Guaíra</option>
		<option value="guaraniacu" <?php if ($dados["grupo"] == 'guaraniacu') echo 'selected="selected"';?>>Guaraniaçu</option>
		<option value="itaipulandia" <?php if ($dados["grupo"] == 'itaipulandia') echo 'selected="selected"';?>>Itaipulândia</option>
		<option value="marechal_candido_rondon" <?php if ($dados["grupo"] == 'marechal_candido_rondon') echo 'selected="selected"';?>>Marechal Cândido Rondon</option>
		<option value="maripa" <?php if ($dados["grupo"] == 'maripa') echo 'selected="selected"';?>>Maripá</option>
		<option value="assama" <?php if ($dados["grupo"] == 'assama') echo 'selected="selected"';?>>ASSAMA</option>
		<option value="mercedes" <?php if ($dados["grupo"] == 'mercedes') echo 'selected="selected"';?>>Mercedes</option>
		<option value="missal" <?php if ($dados["grupo"] == 'missal') echo 'selected="selected"';?>>Missal</option>
		<option value="mundo_novo" <?php if ($dados["grupo"] == 'mundo_novo') echo 'selected="selected"';?>>Mundo Novo</option>
		<option value="nova_santa_rosa" <?php if ($dados["grupo"] == 'nova_santa_rosa') echo 'selected="selected"';?>>Nova Santa Rosa</option>
		<option value="ouro_verde" <?php if ($dados["grupo"] == 'ouro_verde') echo 'selected="selected"';?>>Ouro Verde</option>
		<option value="apava" <?php if ($dados["grupo"] == 'apava') echo 'selected="selected"';?>>APAVA</option>
		<option value="pato_bragado" <?php if ($dados["grupo"] == 'pato_bragado') echo 'selected="selected"';?>>Pato Bragado</option>
		<option value="quatro_pontes" <?php if ($dados["grupo"] == 'quatro_pontes') echo 'selected="selected"';?>>Quatro Pontes</option>
		<option value="ramilandia" <?php if ($dados["grupo"] == 'ramilandia') echo 'selected="selected"';?>>Ramilândia</option>
		<option value="santa_helena" <?php if ($dados["grupo"] == 'santa_helena') echo 'selected="selected"';?>>Santa Helena</option>
		<option value="santa_tereza_do_oeste" <?php if ($dados["grupo"] == 'santa_tereza_do_oeste') echo 'selected="selected"';?>>Santa Tereza do Oeste</option>
		<option value="acaresti" <?php if ($dados["grupo"] == 'acaresti') echo 'selected="selected"';?>>ACARESTI</option>
		<option value="sao_jose_das_palmeiras" <?php if ($dados["grupo"] == 'sao_jose_das_palmeiras') echo 'selected="selected"';?>>São José Das Palmeiras</option>
		<option value="amar" <?php if ($dados["grupo"] == 'amar') echo 'selected="selected"';?>>AMAR</option>
		<option value="sao_pedro_do_igaucu" <?php if ($dados["grupo"] == 'sao_pedro_do_igaucu') echo 'selected="selected"';?>>São Pedro do Iguaçú</option>
		<option value="terra_roxa" <?php if ($dados["grupo"] == 'terra_roxa') echo 'selected="selected"';?>>Terra Roxa</option>
		<option value="toledo" <?php if ($dados["grupo"] == 'toledo') echo 'selected="selected"';?>>Toledo</option>
		<option value="altonia" <?php if ($dados["grupo"] == 'altonia') echo 'selected="selected"';?>>Altônia</option>
		<option value="matelandia" <?php if ($dados["grupo"] == 'matelandia') echo 'selected="selected"';?>>Matelândia</option>
		<option value="vera_cruz_do_oeste" <?php if ($dados["grupo"] == 'vera_cruz_do_oeste') echo 'selected="selected"';?>>Vera Cruz do Oeste</option>
		<option value="catamare" <?php if ($dados["grupo"] == 'catamare') echo 'selected="selected"';?>>Catamare</option>
		<option value="vila_santa_maria" <?php if ($dados["grupo"] == 'vila_santa_maria') echo 'selected="selected"';?>>Vila Santa Maria</option>
		<option value="resol" <?php if ($dados["grupo"] == 'resol') echo 'selected="selected"';?>>Resol</option>
		<option value="acapra" <?php if ($dados["grupo"] == 'acapra') echo 'selected="selected"';?>>Acapra</option>
		<option value="recilapa" <?php if ($dados["grupo"] == 'recilapa') echo 'selected="selected"';?>>Recilapa</option>
		<option value="reciclar" <?php if ($dados["grupo"] == 'reciclar') echo 'selected="selected"';?>>Reciclar</option>
		<option value="santo_anibal" <?php if ($dados["grupo"] == 'santo_anibal') echo 'selected="selected"';?>>Santo Anibal</option>
		<option value="projeto_mutirao" <?php if ($dados["grupo"] == 'projeto_mutirao') echo 'selected="selected"';?>>Projeto Mutirão</option>
		<option value="amar_ebenezer" <?php if ($dados["grupo"] == 'amar_ebenezer') echo 'selected="selected"';?>>Amar Ebenezer</option>
		<option value="jardim_icarai" <?php if ($dados["grupo"] == 'jardim_icarai') echo 'selected="selected"';?>>Jardim Icaraí</option>
		<option value="almirante_tamandare" <?php if ($dados["grupo"] == 'almirante_tamandare') echo 'selected="selected"';?>>Almirante Tamandaré</option>
		<option value="moranguinho" <?php if ($dados["grupo"] == 'moranguinho') echo 'selected="selected"';?>>Moranguinho</option>
		<option value="sociedade_unidade" <?php if ($dados["grupo"] == 'sociedade_unidade') echo 'selected="selected"';?>>Sociedade Unidade</option>
		<option value="coopzumbi" <?php if ($dados["grupo"] == 'coopzumbi') echo 'selected="selected"';?>>Coopzumbi</option>
		<option value="parolim" <?php if ($dados["grupo"] == 'parolim') echo 'selected="selected"';?>>Parolim</option>
		<option value="pantanal" <?php if ($dados["grupo"] == 'pantanal') echo 'selected="selected"';?>>Pantanal</option>
		<option value="savana" <?php if ($dados["grupo"] == 'savana') echo 'selected="selected"';?>>Savana</option>
		<option value="ceram" <?php if ($dados["grupo"] == 'ceram') echo 'selected="selected"';?>>Ceram</option>
		<option value="rio_negro" <?php if ($dados["grupo"] == 'rio_negro') echo 'selected="selected"';?>>Rio Negro</option>
	</select></p>

	<h2>Dados Pessoais</h2>

	<p><label>Nome Completo</label> <input type="text" size="40" name="nome" id="nome" value="<?php echo $dados['nome']; ?>" /></p>

	<p><label>Endereço</label> <input type="text" size="50" name="endereco" id="endereco" value="<?php echo $dados['endereco']; ?>" /></p>
	
	<p>Mora em Curitiba, no Bairro: 
	<select name="bairro" id="bairro">
		<option></option>
		<option value=abranches <?php if ($dados["bairro"] == 'abranches') echo 'selected="selected"';?>>Abranches</option>
		<option value=agua_verde <?php if ($dados["bairro"] == 'agua_verde') echo 'selected="selected"';?>>Água Verde</option>
		<option value=ahu <?php if ($dados["bairro"] == 'ahu') echo 'selected="selected"';?>>Ahú</option>
		<option value=alto_boqueirao <?php if ($dados["bairro"] == 'alto_boqueirao') echo 'selected="selected"';?>>Alto Boqueirão</option>
		<option value=alto_da_gloria <?php if ($dados["bairro"] == 'alto_da_gloria') echo 'selected="selected"';?>>Alto da Glória</option>
		<option value=alto_da_rua_xv <?php if ($dados["bairro"] == 'alto_da_rua_xv') echo 'selected="selected"';?>>Alto da Rua XV</option>
		<option value=atuba <?php if ($dados["bairro"] == 'atuba') echo 'selected="selected"';?>>Atuba</option>
		<option value=augusta <?php if ($dados["bairro"] == 'augusta') echo 'selected="selected"';?>>Augusta</option>
		<option value=bacacheri <?php if ($dados["bairro"] == 'bacacheri') echo 'selected="selected"';?>>Bacacheri</option>
		<option value=bairro_alto <?php if ($dados["bairro"] == 'bairro_alto') echo 'selected="selected"';?>>Bairro Alto</option>
		<option value=barreirinha <?php if ($dados["bairro"] == 'barreirinha') echo 'selected="selected"';?>>Barreirinha</option>
		<option value=batel <?php if ($dados["bairro"] == 'batel') echo 'selected="selected"';?>>Batel</option>
		<option value=bigorrilho <?php if ($dados["bairro"] == 'bigorrilho') echo 'selected="selected"';?>>Bigorrilho</option>
		<option value=boa_vista <?php if ($dados["bairro"] == 'boa_vista') echo 'selected="selected"';?>>Boa Vista</option>
		<option value=bom_retiro <?php if ($dados["bairro"] == 'bom_retiro') echo 'selected="selected"';?>>Bom Retiro</option>
		<option value=boqueirao <?php if ($dados["bairro"] == 'boqueirao') echo 'selected="selected"';?>>Boqueirão</option>
		<option value=butiatuvinha <?php if ($dados["bairro"] == 'butiatuvinha') echo 'selected="selected"';?>>Butiatuvinha</option>
		<option value=cabral <?php if ($dados["bairro"] == 'cabral') echo 'selected="selected"';?>>Cabral</option>
		<option value=cachoeira <?php if ($dados["bairro"] == 'cachoeira') echo 'selected="selected"';?>>Cachoeira</option>
		<option value=cajuru <?php if ($dados["bairro"] == 'cajuru') echo 'selected="selected"';?>>Cajuru</option>
		<option value=campina_grande <?php if ($dados["bairro"] == 'campina_grande') echo 'selected="selected"';?>>Campina do Siqueira</option>
		<option value=campo_comprido <?php if ($dados["bairro"] == 'campo_comprido') echo 'selected="selected"';?>>Campo Comprido</option>
		<option value=campo_de_santana <?php if ($dados["bairro"] == 'campo_de_santana') echo 'selected="selected"';?>>Campo de Santana</option>
		<option value=capao_da_imbuia <?php if ($dados["bairro"] == 'capao_da_imbuia') echo 'selected="selected"';?>>Capão da Imbuia</option>
		<option value=capao_raso <?php if ($dados["bairro"] == 'capao_raso') echo 'selected="selected"';?>>Capão Raso</option>
		<option value=cascatinha <?php if ($dados["bairro"] == 'cascatinha') echo 'selected="selected"';?>>Cascatinha</option>
		<option value=caximba <?php if ($dados["bairro"] == 'caximba') echo 'selected="selected"';?>>Caximba</option>
		<option value=centro <?php if ($dados["bairro"] == 'centro') echo 'selected="selected"';?>>Centro</option>
		<option value=centro_civico <?php if ($dados["bairro"] == 'centro_civico') echo 'selected="selected"';?>>Centro Cívico</option>
		<option value=cidade_industrial_de_curitiba <?php if ($dados["bairro"] == 'cidade_industrial_de_curitiba') echo 'selected="selected"';?>>Cidade Industrial de Curitiba</option>
		<option value=cristo_rei <?php if ($dados["bairro"] == 'cristo_rei') echo 'selected="selected"';?>>Cristo Rei</option>
		<option value=fanny <?php if ($dados["bairro"] == 'fanny') echo 'selected="selected"';?>>Fanny</option>
		<option value=fazendinha <?php if ($dados["bairro"] == 'fazendinha') echo 'selected="selected"';?>>Fazendinha</option>
		<option value=ganchinho <?php if ($dados["bairro"] == 'ganchinho') echo 'selected="selected"';?>>Ganchinho</option>
		<option value=guabirotuba <?php if ($dados["bairro"] == 'guabirotuba') echo 'selected="selected"';?>>Guabirotuba</option>
		<option value=guaira <?php if ($dados["bairro"] == 'guaira') echo 'selected="selected"';?>>Guaíra</option>
		<option value=hauer <?php if ($dados["bairro"] == 'hauer') echo 'selected="selected"';?>>Hauer</option>
		<option value=hugo_lange <?php if ($dados["bairro"] == 'hugo_lange') echo 'selected="selected"';?>>Hugo Lange</option>
		<option value=jardim_botanico <?php if ($dados["bairro"] == 'jardim_botanico') echo 'selected="selected"';?>>Jardim Botânico</option>
		<option value=jardim_das_americas <?php if ($dados["bairro"] == 'jardim_das_americas') echo 'selected="selected"';?>>Jardim das Américas</option>
		<option value=jardim_social <?php if ($dados["bairro"] == 'jardim_social') echo 'selected="selected"';?>>Jardim Social</option>
		<option value=juveve <?php if ($dados["bairro"] == 'juveve') echo 'selected="selected"';?>>Juvevê</option>
		<option value=lamenha_pequena <?php if ($dados["bairro"] == 'lamenha_pequena') echo 'selected="selected"';?>>Lamenha Pequena</option>
		<option value=lindoia <?php if ($dados["bairro"] == 'lindoia') echo 'selected="selected"';?>>Lindóia</option>
		<option value=merces <?php if ($dados["bairro"] == 'merces') echo 'selected="selected"';?>>Mercês</option>
		<option value=mossungue <?php if ($dados["bairro"] == 'mossungue') echo 'selected="selected"';?>>Mossunguê</option>
		<option value=novo_mundo <?php if ($dados["bairro"] == 'novo_mundo') echo 'selected="selected"';?>>Novo Mundo</option>
		<option value=orleans <?php if ($dados["bairro"] == 'orleans') echo 'selected="selected"';?>>Orleans</option>
		<option value=parolin <?php if ($dados["bairro"] == 'parolin') echo 'selected="selected"';?>>Parolin</option>
		<option value=pilarzinho <?php if ($dados["bairro"] == 'pilarzinho') echo 'selected="selected"';?>>Pilarzinho</option>
		<option value=pinheirinho <?php if ($dados["bairro"] == 'pinheirinho') echo 'selected="selected"';?>>Pinheirinho</option>
		<option value=portao <?php if ($dados["bairro"] == 'portao') echo 'selected="selected"';?>>Portão</option>
		<option value=prado_velho <?php if ($dados["bairro"] == 'prado_velho') echo 'selected="selected"';?>>Prado Velho</option>
		<option value=reboucas <?php if ($dados["bairro"] == 'reboucas') echo 'selected="selected"';?>>Rebouças</option>
		<option value=riviera <?php if ($dados["bairro"] == 'riviera') echo 'selected="selected"';?>>Riviera</option>
		<option value=santa_candida <?php if ($dados["bairro"] == 'santa_candida') echo 'selected="selected"';?>>Santa Cândida</option>
		<option value=santa_felicidade <?php if ($dados["bairro"] == 'santa_felicidade') echo 'selected="selected"';?>>Santa Felicidade</option>
		<option value=santa_quiteria <?php if ($dados["bairro"] == 'santa_quiteria') echo 'selected="selected"';?>>Santa Quitéria</option>
		<option value=santo_inacio <?php if ($dados["bairro"] == 'santo_inacio') echo 'selected="selected"';?>>Santo Inácio</option>
		<option value=sao_braz <?php if ($dados["bairro"] == 'sao_braz') echo 'selected="selected"';?>>São Braz</option>
		<option value=sao_francisco <?php if ($dados["bairro"] == 'sao_francisco') echo 'selected="selected"';?>>São Francisco</option>
		<option value=sao_joao <?php if ($dados["bairro"] == 'sao_joao') echo 'selected="selected"';?>>São João</option>
		<option value=sao_lourenco <?php if ($dados["bairro"] == 'sao_lourenco') echo 'selected="selected"';?>>São Lourenço</option>
		<option value=sao_miguel <?php if ($dados["bairro"] == 'sao_miguel') echo 'selected="selected"';?>>São Miguel</option>
		<option value=seminario <?php if ($dados["bairro"] == 'seminario') echo 'selected="selected"';?>>Seminário</option>
		<option value=sitio_cercado <?php if ($dados["bairro"] == 'sitio_cercado') echo 'selected="selected"';?>>Sítio Cercado</option>
		<option value=taboao <?php if ($dados["bairro"] == 'taboao') echo 'selected="selected"';?>>Taboão</option>
		<option value=taruma <?php if ($dados["bairro"] == 'taruma') echo 'selected="selected"';?>>Tarumã</option>
		<option value=tatuquara <?php if ($dados["bairro"] == 'tatuquara') echo 'selected="selected"';?>>Tatuquara</option>
		<option value=tingui <?php if ($dados["bairro"] == 'tingui') echo 'selected="selected"';?>>Tingüi</option>
		<option value=uberaba <?php if ($dados["bairro"] == 'uberaba') echo 'selected="selected"';?>>Uberaba</option>
		<option value=umbara <?php if ($dados["bairro"] == 'umbara') echo 'selected="selected"';?>>Umbará</option>
		<option value=vila_izabel <?php if ($dados["bairro"] == 'vila_izabel') echo 'selected="selected"';?>>Vila Izabel</option>
		<option value=vista_alegre <?php if ($dados["bairro"] == 'vista_alegre') echo 'selected="selected"';?>>Vista Alegre</option>
		<option value=xaxim <?php if ($dados["bairro"] == 'xaxim') echo 'selected="selected"';?>>Xaxim</option>
	</select></p>

	<p>Mora na Região Metropolitana: 
	<select name="regiaometropolitana" id="regiaometropolitana">
		<option></option>
		<option value=adrianopolis <?php if ($dados["regiaometropolitana"] == 'adrianopolis') echo 'selected="selected"';?>>Adrianópolis</option>
		<option value=agudos_do_sul <?php if ($dados["regiaometropolitana"] == 'agudos_do_sul') echo 'selected="selected"';?>>Agudos do Sul</option>
		<option value=almirante_tamandare <?php if ($dados["regiaometropolitana"] == 'almirante_tamandare') echo 'selected="selected"';?>>Almirante Tamandaré</option>
		<option value=araucaria <?php if ($dados["regiaometropolitana"] == 'araucaria') echo 'selected="selected"';?>>Araucária</option>
		<option value=balsa_nova <?php if ($dados["regiaometropolitana"] == 'balsa_nova') echo 'selected="selected"';?>>Balsa Nova</option>
		<option value=bocaiuva_do_sul <?php if ($dados["regiaometropolitana"] == 'bocaiuva_do_sul') echo 'selected="selected"';?>>Bocaiúva do Sul</option>
		<option value=campina_grande_do_sul <?php if ($dados["regiaometropolitana"] == 'campina_grande_do_sul') echo 'selected="selected"';?>>Campina Grande do Sul</option>
		<option value=campo_largo <?php if ($dados["regiaometropolitana"] == 'campo_largo') echo 'selected="selected"';?>>Campo Largo</option>
		<option value=campo_magro <?php if ($dados["regiaometropolitana"] == 'campo_magro') echo 'selected="selected"';?>>Campo Magro</option>
		<option value=cerro_azul <?php if ($dados["regiaometropolitana"] == 'cerro_azul') echo 'selected="selected"';?>>Cerro Azul</option>
		<option value=colombo <?php if ($dados["regiaometropolitana"] == 'colombo') echo 'selected="selected"';?>>Colombo</option>
		<option value=contenda <?php if ($dados["regiaometropolitana"] == 'contenda') echo 'selected="selected"';?>>Contenda</option>
		<option value=doutor_ulysses <?php if ($dados["regiaometropolitana"] == 'doutor_ulysses') echo 'selected="selected"';?>>Doutor Ulysses</option>
		<option value=fazenda_rio_grande <?php if ($dados["regiaometropolitana"] == 'fazenda_rio_grande') echo 'selected="selected"';?>>Fazenda Rio Grande</option>
		<option value=itaperucu <?php if ($dados["regiaometropolitana"] == 'itaperucu') echo 'selected="selected"';?>>Itaperuçu</option>
		<option value=lapa <?php if ($dados["regiaometropolitana"] == 'lapa') echo 'selected="selected"';?>>Lapa</option>
		<option value=mandirituba <?php if ($dados["regiaometropolitana"] == 'mandirituba') echo 'selected="selected"';?>>Mandirituba</option>
		<option value=pinhais <?php if ($dados["regiaometropolitana"] == 'pinhais') echo 'selected="selected"';?>>Pinhais</option>
		<option value=piraquara <?php if ($dados["regiaometropolitana"] == 'piraquara') echo 'selected="selected"';?>>Piraquara</option>
		<option value=quatro_barras <?php if ($dados["regiaometropolitana"] == 'quatro_barras') echo 'selected="selected"';?>>Quatro Barras</option>
		<option value=quitandinha <?php if ($dados["regiaometropolitana"] == 'quitandinha') echo 'selected="selected"';?>>Quitandinha</option>
		<option value=rio_branco_do_sul <?php if ($dados["regiaometropolitana"] == 'rio_branco_do_sul') echo 'selected="selected"';?>>Rio Branco do Sul</option>
		<option value=sao_jose_dos_pinhais <?php if ($dados["regiaometropolitana"] == 'sao_jose_dos_pinhais') echo 'selected="selected"';?>>São José dos Pinhais</option>
		<option value=tijucas_do_sul <?php if ($dados["regiaometropolitana"] == 'tijucas_do_sul') echo 'selected="selected"';?>>Tijucas do Sul</option>
		<option value=tunas_do_parana <?php if ($dados["regiaometropolitana"] == 'tunas_do_parana') echo 'selected="selected"';?>>Tunas do Paraná</option>
	</select></p>

	<p><label >Telefone</label> <input size="20" name="telefone" id="telefone" value="<?php echo $dados['telefone']; ?>"/></p>
	
	<p><label>Data de Nascimento <small>(dd/mm/aaa)</small></label> <input size="10" name="datanascimento" id="datanascimento" value="<?php echo $dados['datanascimento']; ?>" /></p> 

	<p><label>Sexo</label><br/>
	<input type="radio" name="sexo" id="sexo" value="masculino" <?php if ($dados["sexo"] == 'masculino') echo 'checked="checked"';?> /><label>Masculino</label>&#09;
	<input type="radio" name="sexo" id="sexo" value="feminino" <?php if ($dados["sexo"] == 'feminino') echo 'checked="checked"';?> /><label>Feminino</label></p>

	<p><label>Raça</label><br />
		<input type="radio" name="raca" id="raca" value="branca" <?php if ($dados["raca"] == 'branca') echo 'checked="checked"';?> /><label>Branca</label>&#09; 
		<input type="radio" name="raca" id="raca" value="negra" <?php if ($dados["raca"] == 'negra') echo 'checked="checked"';?> /><label>Negra</label>&#09;
		<input type="radio" name="raca" id="raca" value="parda" <?php if ($dados["raca"] == 'parda') echo 'checked="checked"';?> /><label>Parda</label>&#09;
		<input type="radio" name="raca" id="raca" value="amarela" <?php if ($dados["raca"] == 'amarela') echo 'checked="checked"';?> /><label>Amarela</label>&#09;
		<input type="radio" name="raca" id="raca" value="indigena" <?php if ($dados["raca"] == 'indigena') echo 'checked="checked"';?> /><label>Indígena</label></p>

	<p><label>Documentos</label><br />
		<input type="checkbox" name="certidao_de_nascimento" id="certidao_de_nascimento" value="certidao_de_nascimento" <?php if($dados["certidao_de_nascimento"] == 'certidao_de_nascimento') echo 'checked="checked"';?> /><label>Certidão de Nascimento</label> <br />
		<input type="checkbox" name="certidao_de_casamento" id="certidao_de_casamento" value="certidao_de_casamento" <?php if($dados["certidao_de_casamento"] == 'certidao_de_casamento') echo 'checked="checked"';?> /><label>Certidão de Casamento</label> <br /> 
		<input type="checkbox" name="rg" id="rg" value="rg" <?php if($dados["rg"] == 'rg') echo 'checked="checked"';?> /><label>RG</label> <br /> 
		<input type="checkbox" name="cpf" id="cpf" value="cpf" <?php if($dados["cpf"] == 'cpf') echo 'checked="checked"';?> /><label>CPF</label> <br /> 
		<input type="checkbox" name="titulo_de_eleitor" id="titulo_de_eleitor" value="titulo_de_eleitor" <?php if($dados["titulo_de_eleitor"] == 'titulo_de_eleitor') echo 'checked="checked"';?> /><label>Título de Eleitor</label> <br /> 
		<input type="checkbox" name="carteira_de_trabalho" id="carteira_de_trabalho" value="carteira_de_trabalho" <?php if($dados["carteira_de_trabalho"] == 'carteira_de_trabalho') echo 'checked="checked"';?> /><label>Carteira de Trabalho</label> <br /> 
		<input type="checkbox" name="pis" id="pis" value="pis" <?php if($dados["pis"] == 'pis') echo 'checked="checked"';?> /><label>PIS</label></p>
		
		
	<p><label>Desde Que Ano Reside Nesta Cidade <small>(aaaa)</small></label> <input type="text" size="5" id="tempo_de_residencia" name="tempo_de_residencia" value="<?php echo $dados["tempo_de_residencia"]; ?>" /></p>

	<h2>Família</h2>
	
	<p><input type="checkbox" name="chefe_de_familia" id="chefe_de_familia" value="chefe_de_familia" <?php if($dados["chefe_de_familia"] == 'chefe_de_familia') echo 'checked="checked"';?> />&#09;<label>É Chefe de Família</label></p>
  
	<p><label >Número de Filhos</label> <input type="text" size="5" name="filhos" value="<?php echo $dados["filhos"]; ?>" /></p>

	<p><label>Estado Civil</label><br />
	<input type="radio" name="estado_civil" id="estado_civil" value="solteiro" <?php if($dados["estado_civil"] == 'solteiro') echo 'checked="checked"';?> /> <label>Solteiro(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="casado" <?php if($dados["estado_civil"] == 'casado') echo 'checked="checked"';?> /><label>Casado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="amigado" <?php if($dados["estado_civil"] == 'amigado') echo 'checked="checked"';?> /><label>Amigado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="separado" <?php if($dados["estado_civil"] == 'separado') echo 'checked="checked"';?> /><label>Separado(a)</label>&#09;
	<input type="radio" name="estado_civil" id="estado_civil" value="viuvo" <?php if($dados["estado_civil"] == 'viuvo') echo 'checked="checked"';?> /> <label>Viúvo(a)</label>&#09;</p>

	<p>Número de Pessoas na Família<br />
	<label>Que Residem em Conjunto</label> <input type="text" size="5" name="residem_em_conjunto" value="<?php echo $dados["residem_em_conjunto"]; ?>" /><br />
	<label>Menores de 12 anos</label> <input type="text" size="5" name="menores_de_12" value="<?php echo $dados["menores_de_12"]; ?>" /><br />
	<label>Entre 12 e 18 anos</label> <input type="text" size="5" name="entre_12_e_18" value="<?php echo $dados["entre_12_e_18"]; ?>" /><br />
	<label>Que Trabalham</label> <input type="text" size="5" name="que_trabalham" value="<?php echo $dados["que_trabalham"]; ?>" /><br /></p>

	<h2>Moradia</h2>

	<p><label>Classificação da Residência</label><br />
	<input type="radio" name="residencia" id="residencia" value="casa" <?php if($dados["residencia"] == 'casa') echo 'checked="checked"';?> /><label>Casa</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="comodo" <?php if($dados["residencia"] == 'comodo') echo 'checked="checked"';?> /><label>Cômodo</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="pensao" <?php if($dados["residencia"] == 'pensao') echo 'checked="checked"';?> /><label>Pensão</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="lixao" <?php if($dados["residencia"] == 'lixao') echo 'checked="checked"';?> /><label>Lixão</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="deposito" <?php if($dados["residencia"] == 'deposito') echo 'checked="checked"';?> /><label>Depósito</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="albergue" <?php if($dados["residencia"] == 'albergue') echo 'checked="checked"';?> /><label>Albergue</label>&#09;
	<input type="radio" name="residencia" id="residencia" value="rua" <?php if($dados["residencia"] == 'rua') echo 'checked="checked"';?> /> <label>Rua</label><br />
	<label>Outro</label> <input type="text" size="10" name="residencia_outro" value="<?php echo $dados["residencia_outro"]; ?>" /></p>

	<p><label>Situação da Residência</label><br />
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="propria_sem_documentos" <?php if($dados["situacao_da_residencia"] == 'propria_sem_documentos') echo 'checked="checked"';?> /><label>Própria Com Documentos</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="propria_com_documentos" <?php if($dados["situacao_da_residencia"] == 'propria_com_documentos') echo 'checked="checked"';?> /><label>Própria Sem Documentos</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="alugada" <?php if($dados["situacao_da_residencia"] == 'alugada') echo 'checked="checked"';?> /><label>Alugada</label>&#09;
	<input type="radio" name="situacao_da_residencia" id="situacao_da_residencia" value="cedida" <?php if($dados["situacao_da_residencia"] == 'cedida') echo 'checked="checked"';?> /><label>Cedida</label><br />
	<label>Outro</label> <input type="text" size="10" name="situacao_da_residencia_outro" id="situacao_da_residencia_outro" value="<?php echo $dados["situacao_da_residencia_outro"]; ?>" /></p>
	
	<p><label>Tipo de Construção</label><br />
	<input type="radio" name="tipo_de_construcao" id="tipo_de_construcao" value="alvenaria" <?php if($dados["tipo_de_construcao"] == 'alvenaria') echo 'checked="checked"';?> /><label>Alvenaria</label>&#09;
	<input type="radio" name="tipo_de_construcao" id="tipo_de_construcao" value="madeira" <?php if($dados["tipo_de_construcao"] == 'madeira') echo 'checked="checked"';?> /><label>Madeira</label><br />
	<label>Outro</label> <input type="text" size="10" name="tipo_de_construcao_outro" id="tipo_de_construcao" value="<?php echo $dados["tipo_de_construcao_outro"]; ?>" /></p>
	
	<h2>Renda</h2>	
	
	<p><input type="checkbox" name="contribuinte_do_inss" id="contribuinte_do_inss" value="contribuinte_do_inss" <?php if($dados["contribuinte_do_inss"] == 'contribuinte_do_inss') echo 'checked="checked"';?> /> <label>Contribuinte do INSS</label></p>
	
	<p>Não contribui ao INSS por:<br />
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="renda" <?php if($dados["nao_contribui_ao_inss"] == 'renda') echo 'checked="checked"';?> /><label>Falta de Renda</label>&#09;
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="informacoes" <?php if($dados["nao_contribui_ao_inss"] == 'informacoes') echo 'checked="checked"';?> /><label>Falta de Informações</label>&#09;
	<input type="radio" name="nao_contribui_ao_inss" id="nao_contribui_ao_inss" value="nao_quer" <?php if($dados["nao_contribui_ao_inss"] == 'nao_quer') echo 'checked="checked"';?> /><label>Não Querer Contribuir</label><br />
	<label>Outro</label> <input type="text" size="10" name="nao_contribui_ao_inss_outro" id="nao_contribui_ao_inss_outro" value="<?php echo $dados["nao_contribui_ao_inss_outro"]; ?>" /></p>


	<p><label>Renda Familiar Aproximada</label>&#09; R$<input type="text" size="10" name="renda_familiar" value="<?php echo $dados["renda_familiar"]; ?>" id="renda_familiar" onKeyPress="return(currencyFormat(this,',','.',event))"></p>
	
	<p><label>Remuneração</label><br />
	<input type="radio" name="remuneracao" id="remuneracao" value="150" <?php if($dados["remuneracao"] == '150') echo 'checked="checked"';?> /><label>Até R$150,00</label><br />
	<input type="radio" name="remuneracao" id="remuneracao" value="151_e_300" <?php if($dados["remuneracao"] == '151_e_300') echo 'checked="checked"';?> /><label>Entre R$151,00 e R$300,00</label> <br />
	<input type="radio" name="remuneracao" id="remuneracao" value="301_e_450" <?php if($dados["remuneracao"] == '301_e_450') echo 'checked="checked"';?> /><label>Entre R$301,00 e R$450,00</label> <br /> 
	<input type="radio" name="remuneracao" id="remuneracao" value="451_e_600" <?php if($dados["remuneracao"] == '451_e_600') echo 'checked="checked"';?> /><label>Entre R$451,00 e R$600,00</label> <br /> 
	<input type="radio" name="remuneracao" id="remuneracao" value="600" <?php if($dados["remuneracao"] == '600') echo 'checked="checked"';?> /><label>Acima de R$600,00</label></p>

	<p><label>Água Encanada</label><br />
	<input type="radio" name="agua_encanada" id="agua_encanada" value="sanepar" <?php if($dados["agua_encanada"] == 'sanepar') echo 'checked="checked"';?> /><label>Sanepar</label>&#09;
	<input type="radio" name="agua_encanada" id="agua_encanada" value="informal" <?php if($dados["agua_encanada"] == 'informal') echo 'checked="checked"';?> /><label>Informal</label>&#09;
	<input type="radio" name="agua_encanada" id="agua_encanada" value="nao_possui" <?php if($dados["agua_encanada"] == 'nao_possui') echo 'checked="checked"';?> /><label>Não Possui</label></p>

	<p><label>Energia Elétrica</label><br />
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="copel" <?php if($dados["energia_eletrica"] == 'copel') echo 'checked="checked"';?> /><label>Copel</label>&#09;
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="informal" <?php if($dados["energia_eletrica"] == 'informal') echo 'checked="checked"';?> /><label>Informal</label>&#09;
	<input type="radio" name="energia_eletrica" id="energia_eletrica" value="nao_possui" <?php if($dados["energia_eletrica"] == 'nao_possui') echo 'checked="checked"';?> /><label>Não Possui</label></p>

	<p><label>Esgoto</label><br />
	<input type="radio" name="esgoto" id="esgoto" value="rede_publica" <?php if($dados["esgoto"] == 'rede_publica') echo 'checked="checked"';?> /><label>Rede Pública</label>&#09;
	<input type="radio" name="esgoto" id="esgoto" value="fossa" <?php if($dados["esgoto"] == 'fossa') echo 'checked="checked"';?> /><label>Fossa</label>&#09;
	<input type="radio" name="esgoto" id="esgoto" value="ceu_aberto" <?php if($dados["esgoto"] == 'ceu_aberto') echo 'checked="checked"';?> /><label>Céu Aberto</label></p>

	<p><input type="checkbox" name="possui_banheiro" id="possui_banheiro" value="possui_banheiro" <?php if($dados["possui_banheiro"] == 'possui_banheiro') echo 'checked="checked"';?> /><label>Possui Banheiro</label></p>
	
	<h2>Estudos</h2>
	
	<p><label>Estudou Até a</label>
	<select name="estudou_ate" id="estudou_ate">
		<option></option>
		<option value="1a_serie" <?php if ($dados["estudou_ate"] == '1a_serie') echo 'selected="selected"';?>>1&ordf; Série</option>
		<option value="2a_serie" <?php if ($dados["estudou_ate"] == '2a_serie') echo 'selected="selected"';?>>2&ordf; Série</option>
		<option value="3a_serie" <?php if ($dados["estudou_ate"] == '3a_serie') echo 'selected="selected"';?>>3&ordf; Série</option>
		<option value="4a_serie" <?php if ($dados["estudou_ate"] == '4a_serie') echo 'selected="selected"';?>>4&ordf; Série</option>
		<option value="5a_serie" <?php if ($dados["estudou_ate"] == '5a_serie') echo 'selected="selected"';?>>5&ordf; Série</option>
		<option value="6a_serie" <?php if ($dados["estudou_ate"] == '6a_serie') echo 'selected="selected"';?>>6&ordf; Série</option>
		<option value="7a_serie" <?php if ($dados["estudou_ate"] == '7a_serie') echo 'selected="selected"';?>>7&ordf; Série</option>
		<option value="8a_serie" <?php if ($dados["estudou_ate"] == '8a_serie') echo 'selected="selected"';?>>8&ordf; Série</option>
		<option value="1a_serie_2o_grau" <?php if ($dados["estudou_ate"] == '1a_serie_2o_grau') echo 'selected="selected"';?>>1&ordf; Série do Segundo Grau</option>
		<option value="2a_serie_2o_grau" <?php if ($dados["estudou_ate"] == '2a_serie_2o_grau') echo 'selected="selected"';?>>2&ordf; Série do Segundo Grau</option>
		<option value="3a_serie_2o_grau" <?php if ($dados["estudou_ate"] == '3a_serie_2o_grau') echo 'selected="selected"';?>>3&ordf; Série do Segundo Grau</option>
	</select></p>

	<p><label>Gostaria de Estudar</label>
	<input type="checkbox" name="supletivo" id="supletivo" value="supletivo" <?php if($dados["supletivo"] == 'supletivo') echo 'checked="checked"';?> /><label>Supletivo</label>&#09;
	<input type="checkbox" name="informatica" id="informatica" value="informatica" <?php if($dados["informatica"] == 'informatica') echo 'checked="checked"';?> /><label>Informática</label>&#09;
	<input type="checkbox" name="reciclagem" id="reciclagem" value="reciclagem" <?php if($dados["reciclagem"] == 'reciclagem') echo 'checked="checked"';?> /><label>Reciclagem</label>&#09;
	<input type="checkbox" name="alfabetizacao" id="alfabetizacao" value="alfabetizacao" <?php if($dados["alfabetizacao"] == 'alfabetizacao') echo 'checked="checked"';?> /><label>Alfabetização</label>&#09;
	<input type="checkbox" name="gestao" id="gestao" value="gestao" <?php if($dados["gestao"] == 'gestao') echo 'checked="checked"';?> /><label>Gestão de Empreendimentos</label>&#09;
	<input type="checkbox" name="artesanato" id="artesanato" value="artesanato" <?php if($dados["artesanato"] == 'artesanato') echo 'checked="checked"';?> /><label>Artesanato</label><br />
	<label>Outro</label> <input type="text" size="20" name="estudar_outro" id="estudar_outro" value="<?php echo $dados["estudar_outro"]; ?>" /></p>

	<h2>Saúde</h2>

	<p><input type="checkbox" name="fuma" id="fuma" value="fuma" <?php if($dados["fuma"] == 'fuma') echo 'checked="checked"';?> /><label>Fuma</label>&#09;
	<input type="checkbox" name="bebe" id="bebe" value="bebe" <?php if($dados["bebe"] == 'bebe') echo 'checked="checked"';?> /><label>Bebe</label>&#09;
	<input type="checkbox" name="drogas" id="drogas" value="drogas" <?php if($dados["drogas"] == 'drogas') echo 'checked="checked"';?> /><label>Usa Drogas</label><br /></p>
	
	<p>Parou de fumar no ano de <small>(aaaa)</small> <input type="text" size="5" name="fumava" id="fumava" value="<?php echo $dados["fumava"]; ?>" /></p>
	
	<p>Parou de beber no ano de <small>(aaaa)</small> <input type="text" size="5" name="bebia" id="bebia" value="<?php echo $dados["bebia"]; ?>" /></p>
	
	<p>Parou de usar drogas no ano de <small>(aaaa)</small> <input type="text" size="5" name="usava_drogas" id="usava_drogas" value="<?php echo $dados["usava_drogas"]; ?>" /></p>
	
	<p><input type="checkbox" name="frequenta_medico" id="frequenta_medico" value="frequenta_medico" <?php if($dados["frequenta_medico"] == 'frequenta_medico') echo 'checked="checked"';?> /><label>Freqüenta Médico Regularmente</label><br />
	<input type="checkbox" name="faz_tratamento_de_saude" id="faz_tratamento_de_saude" value="faz_tratamento_de_saude" <?php if($dados["faz_tratamento_de_saude"] == 'faz_tratamento_de_saude') echo 'checked="checked"';?> /><label>Faz Tratamento de Saúde</label><br />
	<input type="checkbox" name="toma_medicamentos" id="toma_medicamentos" value="toma_medicamentos" <?php if($dados["toma_medicamentos"] == 'toma_medicamentos') echo 'checked="checked"';?> /><label>Toma Medicamentos Regularmente</label><br />	
	<input type="checkbox" name="faz_acompanhamento_psicologico" id="faz_acompanhamento_psicologico" value="faz_acompanhamento_psicologico" <?php if($dados["faz_acompanhamento_psicologico"] == 'faz_acompanhamento_psicologico') echo 'checked="checked"';?> /><label>Faz Acompanhamento Psicológico</label><br />	
	<input type="checkbox" name="frequenta_dentista" id="frequenta_dentista" value="frequenta_dentista" <?php if($dados["frequenta_dentista"] == 'frequenta_dentista') echo 'checked="checked"';?> /><label>Freqüenta Dentista Regularmente</label></p>
	
	<p><label>Como é o atendimento do Posto de Saúde da Sua Região?</label><br />
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="otimo" <?php if($dados["atendimento_posto_de_saude"] == 'otimo') echo 'checked="checked"';?> /><label>Ótimo</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="bom" <?php if($dados["atendimento_posto_de_saude"] == 'bom') echo 'checked="checked"';?> /> <label>Bom</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="regular" <?php if($dados["atendimento_posto_de_saude"] == 'regular') echo 'checked="checked"';?> /><label>Regular</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="ruim" <?php if($dados["atendimento_posto_de_saude"] == 'ruim') echo 'checked="checked"';?> /><label>Ruim</label>&#09;
	<input type="radio" name="atendimento_posto_de_saude" id="atendimento_posto_de_saude" value="pessimo" <?php if($dados["atendimento_posto_de_saude"] == 'pessimo') echo 'checked="checked"';?> /><label>Péssimo</label> </p>

	<p><input type="checkbox" name="conhece_psf" id="conhece_psf" value="conhece_psf" <?php if($dados["conhece_psf"] == 'conhece_psf') echo 'checked="checked"';?> /><label>Conhece o Programa Saúde da Família em Sua Região</label><br />
	<input type="checkbox" name="ja_foi_atendido_psf" id="ja_foi_atendido_psf" value="ja_foi_atendido_psf" <?php if($dados["ja_foi_atendido_psf"] == 'ja_foi_atendido_psf') echo 'checked="checked"';?> /><label>Já Foi Atendido Pelo Programa Saúde da Família</label></p>

	<p><input type="checkbox" name="conhece_conselho_saude_local" id="conhece_conselho_saude_local" value="conhece_conselho_saude_local" <?php if($dados["conhece_conselho_saude_local"] == 'conhece_conselho_saude_local') echo 'checked="checked"';?> /><label>Conhece o Conselho de Saúde Local</label><br />
	<input type="checkbox" name="participou_conselho_saude_local" id="participou_conselho_saude_local" value="participou_conselho_saude_local" <?php if($dados["participou_conselho_saude_local"] == 'participou_conselho_saude_local') echo 'checked="checked"';?> /><label>Já Participou do Conselho de Saúde Local</label></p>

	<h2>Trabalho</h2>
	
	<p><label>Religião</label>
	<select name="religiao" id="religiao" type="varchar">
		<option></option>
		<option value="evangelica" <?php if ($dados["religiao"] == 'evangelica') echo 'selected="selected"';?>>Evangélica</option>
		<option value="catolica" <?php if ($dados["religiao"] == 'catolica') echo 'selected="selected"';?>>Católica</option>
	</select></p>
	
	<p><label>Trabalhou em Outra Coisa Antes de Ser Catador:</label> <input type="text" size="15" name="trabalhou_antes" value="<?php echo $dados["trabalhou_antes"]; ?>" /></p>
	
	<p><input type="checkbox" name="trabalhou_carteira_assinada" id="trabalhou_carteira_assinada" value="trabalhou_carteira_assinada" <?php if($dados["trabalhou_carteira_assinada"] == 'trabalhou_carteira_assinada') echo 'checked="checked"';?> /> <label>Já Trabalhou de Carteira Assinada</label></p>

	<p><label>É Catador Desde o Ano de <small>(aaaa)</small></label> <input type="text" size="5" name="tempo_que_e_catador" value="<?php echo $dados["tempo_que_e_catador"]; ?>" /></p>

	<p><input type="checkbox" name="sofreu_acidente" id="sofreu_acidente" value="sofreu_acidente" <?php if($dados["sofreu_acidente"] == 'sofreu_acidente') echo 'checked="checked"';?> /> <label>Já Sofreu Acidente de Trânsito Como Catador</label></p>
	
	<p><label>Tem Outro Emprego Além de Catador:</label> <input type="text" size="15" name="outro_emprego" value="<?php echo $dados["outro_emprego"]; ?>" /></p>

	<p>Benefícios Pessoais Recebidos:<br />
	<input type="checkbox" name="pensao" id="pensao" value="pensao" <?php if($dados["pensao"] == 'pensao') echo 'checked="checked"';?> /><label>Pensão</label>&#09;
	<input type="checkbox" name="aposentadoria" id="aposentadoria" value="aposentadoria" <?php if($dados["aposentadoria"] == 'aposentadoria') echo 'checked="checked"';?> /><label>Aposentadoria</label>&#09;
	<input type="checkbox" name="seguro_desemprego" id="seguro_desemprego" value="seguro_desemprego" <?php if($dados["seguro_desemprego"] == 'seguro_desemprego') echo 'checked="checked"';?> /><label>Seguro Desemprego</label>&#09;
	<input type="checkbox" name="auxilio_doenca" id="auxilio_doenca" value="auxilio_doenca" <?php if($dados["auxilio_doenca"] == 'auxilio_doenca') echo 'checked="checked"';?> /><label>Auxílio Doença</label>&#09;
	<input type="checkbox" name="beneficio_de_prestacao_continuada" id="beneficio_de_prestacao_continuada" value="beneficio_de_prestacao_continuada" <?php if($dados["beneficio_de_prestacao_continuada"] == 'beneficio_de_prestacao_continuada') echo 'checked="checked"';?> /><label>Benefício de Prestação Continuada</label><br />
	<label>Outro</label> <input type="text" size="10" name="beneficio_outro" value="<?php echo $dados["beneficio_outro"]; ?>" /></p>

	<p>Benefícios Familiares Recebidos:<br />
	<input type="checkbox" name="pensao_familia" id="pensao_familia" value="pensao_familia" <?php if($dados["pensao_familia"] == 'pensao_familia') echo 'checked="checked"';?> /><label>Pensão</label>&#09;
	<input type="checkbox" name="aposentadoria_familia" id="aposentadoria_familia" value="aposentadoria_familia" <?php if($dados["aposentadoria_familia"] == 'aposentadoria_familia') echo 'checked="checked"';?> /><label>Aposentadoria</label>&#09;
	<input type="checkbox" name="seguro_desemprego_familia" id="seguro_desemprego_familia" value="seguro_desemprego_familia" <?php if($dados["seguro_desemprego_familia"] == 'seguro_desemprego_familia') echo 'checked="checked"';?> /><label>Seguro Desemprego</label>&#09;
	<input type="checkbox" name="auxilio_doenca_familia" id="auxilio_doenca_familia" value="auxilio_doenca_familia" <?php if($dados["auxilio_doenca_familia"] == 'auxilio_doenca_familia') echo 'checked="checked"';?> /><label>Auxílio Doença</label>&#09;
	<input type="checkbox" name="beneficio_de_prestacao_continuada_familia" id="beneficio_de_prestacao_continuada_familia" value="beneficio_de_prestacao_continuada_familia" <?php if($dados["beneficio_de_prestacao_continuada_familia"] == 'beneficio_de_prestacao_continuada_familia') echo 'checked="checked"';?> /><label>Benefício de Prestação Continuada</label><br />
	<label>Outro</label> <input type="text" size="10" name="beneficio_familia_outro" value="<?php echo $dados["beneficio_familia_outro"]; ?>" /></p>
	
	<p>Programas Sociais em Que a Família Está Envolvida:<br />
	<input type="checkbox" name="peti" id="peti" value="peti" <?php if($dados["peti"] == 'peti') echo 'checked="checked"';?> /><label>PETI</label>&#09;
	<input type="checkbox" name="bolsa_familia" id="bolsa_familia" value="bolsa_familia" <?php if($dados["bolsa_familia"] == 'bolsa_familia') echo 'checked="checked"';?> /><label>Bolsa Família</label>&#09;
	<input type="checkbox" name="programa_do_leite" id="programa_do_leite" value="programa_do_leite" <?php if($dados["programa_do_leite"] == 'programa_do_leite') echo 'checked="checked"';?> /><label>Programa do leite</label>&#09;
	<input type="checkbox" name="luz_fraterna" id="luz_fraterna" value="luz_fraterna" <?php if($dados["luz_fraterna"] == 'luz_fraterna') echo 'checked="checked"';?> /><label>Luz Fraterna</label>&#09;
	<input type="checkbox" name="tarifa_social_da_agua" id="tarifa_social_da_agua" value="tarifa_social_da_agua" <?php if($dados["tarifa_social_da_agua"] == 'tarifa_social_da_agua') echo 'checked="checked"';?> /><label>Tarifa Social da Água</label>&#09;
	<input type="checkbox" name="agente_jovem" id="agente_jovem" value="agente_jovem" <?php if($dados["agente_jovem"] == 'agente_jovem') echo 'checked="checked"';?> /><label>Agente Jovem</label><br />
	<label>Outro</label> <input type="text" size="10" name="programas_sociais_outro" value="<?php echo $dados["programas_sociais_outro"]; ?>" /><br />
	<label>Doações</label> <input type="text" size="10" name="doacoes" value="<?php echo $dados["doacoes"]; ?>" /></p>

	<p>Distância do grupo, associação ou cooperativa <small>(em metros)</small><input type="text" size="5" name="distancia" value="<?php echo $dados["distancia"]; ?>" /></p>

	<p><label>Como normalmente se locomove ao trabalho:</label><br />
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="a_pe" <?php if($dados["locomocao_ao_trabalho"] == 'a_pe') echo 'checked="checked"';?> /><label>A Pé</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="puxando_carrinho" <?php if($dados["locomocao_ao_trabalho"] == 'puxando_carrinho') echo 'checked="checked"';?> /> <label>Puxando o Carrinho</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="onibus" <?php if($dados["locomocao_ao_trabalho"] == 'onibus') echo 'checked="checked"';?> /><label>Ônibus</label>&#09;
	<input type="radio" name="locomocao_ao_trabalho" id="locomocao_ao_trabalho" value="carro" <?php if($dados["locomocao_ao_trabalho"] == 'carro') echo 'checked="checked"';?> /><label>Carro</label><br />
	<label>Outro</label> <input type="text" size="10" name="locomocao_ao_trabalho_outro" value="<?php echo $dados["locomocao_ao_trabalho_outro"]; ?>" /></p>

	<h2>Organização Trabalhista</h2>
	
	<p><label>Ocupa Algum Cargo na Diretoria da Associação? Qual?</label> <input type="text" size="10" name="cargo" value="<?php echo $dados["cargo"]; ?>" /></p>
	
	<p>Conhece as Seguintes Orgaizações:<br />
	<input type="checkbox" name="mncr" id="mncr" value="mncr" <?php if($dados["mncr"] == 'mncr') echo 'checked="checked"';?> /><label>Movimento Nacional de Catadores de Material Reciclável</label><br />
	<input type="checkbox" name="instituto_lixo_cidadania" id="instituto_lixo_cidadania" value="instituto_lixo_cidadania" <?php if($dados["instituto_lixo_cidadania"] == 'instituto_lixo_cidadania') echo 'checked="checked"';?> /><label>Instituto Lixo e Cidadania</label><br />
	<input type="checkbox" name="outras_associacoes" id="outras_associacoes" value="outras_associacoes" <?php if($dados["outras_associacoes"] == 'outras_associacoes') echo 'checked="checked"';?> /><label>Outras Associações de Catadores</label><br />
	<label>Outro</label> <input type="text" size="10" name="organizacoes_outro" value="<?php echo $dados["organizacoes_outro"]; ?>" /></p>

	<p><label>Já Participou de Algum Encontro de Catadores? Qual? </label> 
	<input type="text" size="25" name="participou_de_encontro" value="<?php echo $dados["participou_de_encontro"]; ?>" /></p>
	
	<p><label>Condições de Trabalho Após Filiação com a Associação</label><br />
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="melhores" <?php if($dados["condicoes_apos_filiacao"] == 'melhores') echo 'checked="checked"';?> /><label>Melhores</label>&#09;
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="piores" <?php if($dados["condicoes_apos_filiacao"] == 'piores') echo 'checked="checked"';?> /><label>Piores</label>&#09;	
	<input type="radio" name="condicoes_apos_filiacao" id="condicoes_apos_filiacao" value="nao_mudaram" <?php if($dados["condicoes_apos_filiacao"] == 'nao_mudaram') echo 'checked="checked"';?> /><label>Não Mudaram</label><br />
	<label>Porquê?</label> <input type="text" size="20" name="condicoes_apos_filiacao_porque" value="<?php echo $dados["condicoes_apos_filiacao_porque"]; ?>" /></p>
	
	<p><input type="checkbox" name="aumento_de_rendimentos" id="aumento_de_rendimentos" value="aumento_de_rendimentos" <?php if($dados["aumento_de_rendimentos"] == 'aumento_de_rendimentos') echo 'checked="checked"';?> /><label>Teve Aumento de Rendimentos Após Filiação</label></p>
	
	<p>Vantagens da Filiação<br />
	<input type="checkbox" name="vantagem_remuneracao" id="vantagem_remuneracao" value="vantagem_remuneracao" <?php if($dados["vantagem_remuneracao"] == 'vantagem_remuneracao') echo 'checked="checked"';?> /><label>Remuneração</label>&#09;
	<input type="checkbox" name="vantagem_equipamentos_de_trabalho" id="vantagem_equipamentos_de_trabalho" value="vantagem_equipamentos_de_trabalho" <?php if($dados["vantagem_equipamentos_de_trabalho"] == 'vantagem_equipamentos_de_trabalho') echo 'checked="checked"';?> /><label>Equipamentos de Trabalho</label>&#09;
	<input type="checkbox" name="vantagem_carga_horaria" id="vantagem_carga_horaria" value="vantagem_carga_horaria" <?php if($dados["vantagem_carga_horaria"] == 'vantagem_carga_horaria') echo 'checked="checked"';?> /><label>Carga Horária</label>&#09;
	<input type="checkbox" name="vantagem_cursos" id="vantagem_cursos" value="vantagem_cursos" <?php if($dados["vantagem_cursos"] == 'vantagem_cursos') echo 'checked="checked"';?> /><label>Cursos</label>&#09;
	<input type="checkbox" name="vantagem_doacoes" id="vantagem_doacoes" value="vantagem_doacoes" <?php if($dados["vantagem_doacoes"] == 'vantagem_doacoes') echo 'checked="checked"';?> /><label>Doações</label>&#09;
	<input type="checkbox" name="vantagem_organizacao_da_comunidade" id="vantagem_organizacao_da_comunidade" value="vantagem_organizacao_da_comunidade" <?php if($dados["vantagem_organizacao_da_comunidade"] == 'vantagem_organizacao_da_comunidade') echo 'checked="checked"';?> /><label>Organização da Comunidade</label>&#09;
	<input type="checkbox" name="vantagem_participacao_da_familia" id="vantagem_participacao_da_familia" value="vantagem_participacao_da_familia" <?php if($dados["vantagem_participacao_da_familia"] == 'vantagem_participacao_da_familia') echo 'checked="checked"';?> /><label>Participação da Família</label>&#09;
	<input type="checkbox" name="vantagem_contato_com_instituicoes_de_apoio" id="vantagem_contato_com_instituicoes_de_apoio" value="vantagem_contato_com_instituicoes_de_apoio" <?php if($dados["vantagem_contato_com_instituicoes_de_apoio"] == 'vantagem_contato_com_instituicoes_de_apoio') echo 'checked="checked"';?> /><label>Contato com Instituições de Apoio</label></p>
	
	<p><input type="checkbox" name="preco_de_venda" id="preco_de_venda" value="preco_de_venda" <?php if($dados["preco_de_venda"] == 'preco_de_venda') echo 'checked="checked"';?> /><label>Sabe o Preço de Venda dos Recicláveis</label></p>
	
	<p>Quem Mais Na Família é Catador?<br />
	<input type="checkbox" name="catador_pais" id="catador_pais" value="catador_pais" <?php if($dados["catador_pais"] == 'catador_pais') echo 'checked="checked"';?> /><label>Pais</label>&#09;
	<input type="checkbox" name="catador_irmaos" id="catador_irmaos" value="catador_irmaos" <?php if($dados["catador_irmaos"] == 'catador_irmaos') echo 'checked="checked"';?> /><label>Irmãos</label>&#09;
	<input type="checkbox" name="catador_esposo" id="catador_esposo" value="catador_esposo" <?php if($dados["catador_esposo"] == 'catador_esposo') echo 'checked="checked"';?> /><label>Esposo(a)</label>&#09;
	<input type="checkbox" name="catador_filhos" id="catador_filhos" value="catador_filhos" <?php if($dados["catador_filhos"] == 'catador_filhos') echo 'checked="checked"';?> /><label>Filhos</label><br />
	<label>Outros</label> <input type="text" size="15" name="catador_outro" value="<?php echo $dados["catador_outro"]; ?>" /></p>
	
	<p><label>Como Considera a Condição Familiar</label><br />
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="otima" <?php if($dados["condicao_familiar"] == 'otima') echo 'checked="checked"';?> /><label>Ótima</label>&#09;
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="boa" <?php if($dados["condicao_familiar"] == 'boa') echo 'checked="checked"';?> /><label>Boa</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="regular" <?php if($dados["condicao_familiar"] == 'regular') echo 'checked="checked"';?> /><label>Regular</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="ruim" <?php if($dados["condicao_familiar"] == 'ruim') echo 'checked="checked"';?> /><label>Ruim</label>&#09;	
	<input type="radio" name="condicao_familiar" id="condicao_familiar" value="pessima" <?php if($dados["condicao_familiar"] == 'pessima') echo 'checked="checked"';?> /><label>Péssima</label></p>

	<p>Maiores Dificuldades Encontradas no Trabalho<br />
	<input type="checkbox" name="falta_de_organizacao" id="falta_de_organizacao" value="falta_de_organizacao" <?php if($dados["falta_de_organizacao"] == 'falta_de_organizacao') echo 'checked="checked"';?> /><label>Falta de Organização</label>&#09;
	<input type="checkbox" name="distancias" id="distancias" value="distancias" <?php if($dados["distancias"] == 'distancias') echo 'checked="checked"';?> /><label>Distâncias Para Trabalhar</label>&#09;
	<input type="checkbox" name="inseguranca_financeira" id="inseguranca_financeira" value="inseguranca_financeira" <?php if($dados["inseguranca_financeira"] == 'inseguranca_financeira') echo 'checked="checked"';?> /><label>Insegurança Financeira</label>&#09;
	<input type="checkbox" name="baixa_remuneracao" id="baixa_remuneracao" value="baixa_remuneracao" <?php if($dados["baixa_remuneracao"] == 'baixa_remuneracao') echo 'checked="checked"';?> /><label>Baixa Remuneração</label>&#09;
	<input type="checkbox" name="relacionamento_catadores" id="relacionamento_catadores" value="relacionamento_catadores" <?php if($dados["relacionamento_catadores"] == 'relacionamento_catadores') echo 'checked="checked"';?> /><label>Relacionamento com Outros Catadores</label>&#09;
	<input type="checkbox" name="relacionamento_comunidade" id="relacionamento_comunidade" value="relacionamento_comunidade" <?php if($dados["relacionamento_comunidade"] == 'relacionamento_comunidade') echo 'checked="checked"';?> /><label>Relacionamento com a Comunidade</label>&#09;
	<input type="checkbox" name="falta_de_equipamentos" id="falta_de_equipamentos" value="falta_de_equipamentos" <?php if($dados["falta_de_equipamentos"] == 'falta_de_equipamentos') echo 'checked="checked"';?> /><label>Falta de Equipamentos</label></p>
	
	<p>Quem Considera Líderes do Grupo<br />
	<label>Líder 1:</label> <input type="text" size="25" name="lider_1" value="<?php echo $dados["lider_1"]; ?>" /><br />
	<label>Líder 2:</label> <input type="text" size="25" name="lider_2" value="<?php echo $dados["lider_2"]; ?>" /></p>
	
	<input type="submit" value="Salvar" />
</form>
<?php include("rodape.php"); ?>
