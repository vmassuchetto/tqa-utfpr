<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
$data = $_POST["data"];
$grupo = $_POST["grupo"];
$nome = $_POST["nome"];
$endereco = $_POST["endereco"];
$bairro = $_POST["bairro"];
$regiaometropolitana = $_POST["regiaometropolitana"];
$telefone = $_POST["telefone"];
$datanascimento = $_POST["datanascimento"];
$sexo = $_POST["sexo"];
$raca = $_POST["raca"];
$certidao_de_nascimento = $_POST["certidao_de_nascimento"];
$certidao_de_casamento = $_POST["certidao_de_casamento"];
$rg = $_POST["rg"];
$cpf = $_POST["cpf"];
$titulo_de_eleitor = $_POST["titulo_de_eleitor"];
$carteira_de_trabalho = $_POST["carteira_de_trabalho"];
$pis = $_POST["pis"];
$nascido_em = $_POST["nascido_em"];
$cidade_natal = $_POST["cidade_natal"];
$tempo_de_residencia = $_POST["tempo_de_residencia"];
$chefe_de_familia = $_POST["chefe_de_familia"];
$filhos = $_POST["filhos"];
$estado_civil = $_POST["estado_civil"];
$residem_em_conjunto = $_POST["residem_em_conjunto"];
$menores_de_12 = $_POST["menores_de_12"];
$entre_12_e_18 = $_POST["entre_12_e_18"];
$que_trabalham = $_POST["que_trabalham"];
$residencia = $_POST["residencia"];
$residencia_outro = $_POST["residencia_outro"];
$situacao_da_residencia = $_POST["situacao_da_residencia"];
$situacao_da_residencia_outro = $_POST["situacao_da_residencia_outro"];
$tipo_de_construcao = $_POST["tipo_de_construcao"];
$tipo_de_construcao_outro = $_POST["tipo_de_construcao_outro"];
$contribuinte_do_inss = $_POST["contribuinte_do_inss"];
$nao_contribui_ao_inss = $_POST["nao_contribui_ao_inss"];
$nao_contribui_ao_inss_outro = $_POST["nao_contribui_ao_inss_outro"];
$renda_familiar = $_POST["renda_familiar"];
$remuneracao = $_POST["remuneracao"];
$agua_encanada = $_POST["agua_encanada"];
$energia_eletrica = $_POST["energia_eletrica"];
$esgoto = $_POST["esgoto"];
$possui_banheiro = $_POST["possui_banheiro"];
$estudou_ate = $_POST["estudou_ate"];
$supletivo = $_POST["supletivo"];
$informatica = $_POST["informatica"];
$reciclagem = $_POST["reciclagem"];
$alfabetizacao = $_POST["alfabetizacao"];
$gestao = $_POST["gestao"];
$artesanato = $_POST["artesanato"];
$estudar_outro = $_POST["estudar_outro"];
$fuma = $_POST["fuma"];
$bebe = $_POST["bebe"];
$drogas = $_POST["drogas"];
$fumava = $_POST["fumava"];
$bebia = $_POST["bebia"];
$usava_drogas = $_POST["usava_drogas"];
$frequenta_medico = $_POST["frequenta_medico"];
$faz_tratamento_de_saude = $_POST["faz_tratamento_de_saude"];
$toma_medicamentos = $_POST["toma_medicamentos"];
$faz_acompanhamento_psicologico = $_POST["faz_acompanhamento_psicologico"];
$frequenta_dentista = $_POST["frequenta_dentista"];
$atendimento_posto_de_saude = $_POST["atendimento_posto_de_saude"];
$conhece_psf = $_POST["conhece_psf"];
$ja_foi_atendido_psf = $_POST["ja_foi_atendido_psf"];
$conhece_conselho_saude_local = $_POST["conhece_conselho_saude_local"];
$participou_conselho_saude_local = $_POST["participou_conselho_saude_local"];
$religiao = $_POST["religiao"];
$trabalhou_antes = $_POST["trabalhou_antes"];
$trabalhou_carteira_assinada = $_POST["trabalhou_carteira_assinada"];
$tempo_que_e_catador = $_POST["tempo_que_e_catador"];
$sofreu_acidente = $_POST["sofreu_acidente"];
$outro_emprego = $_POST["outro_emprego"];
$pensao = $_POST["pensao"];
$aposentadoria = $_POST["aposentadoria"];
$seguro_desemprego = $_POST["seguro_desemprego"];
$auxilio_doenca = $_POST["auxilio_doenca"];
$beneficio_de_prestacao_continuada = $_POST["beneficio_de_prestacao_continuada"];
$beneficio_outro = $_POST["beneficio_outro"];
$pensao_familia = $_POST["pensao_familia"];
$aposentadoria_familia = $_POST["aposentadoria_familia"];
$seguro_desemprego_familia = $_POST["seguro_desemprego_familia"];
$auxilio_doenca_familia = $_POST["auxilio_doenca_familia"];
$beneficio_de_prestacao_continuada_familia = $_POST["beneficio_de_prestacao_continuada_familia"];
$beneficio_familia_outro = $_POST["beneficio_familia_outro"];
$peti = $_POST["peti"];
$bolsa_familia = $_POST["bolsa_familia"];
$programa_do_leite = $_POST["programa_do_leite"];
$luz_fraterna = $_POST["luz_fraterna"];
$tarifa_social_da_agua = $_POST["tarifa_social_da_agua"];
$agente_jovem = $_POST["agente_jovem"];
$programas_sociais_outro = $_POST["programas_sociais_outro"];
$doacoes = $_POST["doacoes"];
$distancia = $_POST["distancia"];
$locomocao_ao_trabalho = $_POST["locomocao_ao_trabalho"];
$locomocao_ao_trabalho_outro = $_POST["locomocao_ao_trabalho_outro"];
$cargo = $_POST["cargo"];
$mncr = $_POST["mncr"];
$instituto_lixo_cidadania = $_POST["instituto_lixo_cidadania"];
$outras_associacoes = $_POST["outras_associacoes"];
$organizacoes_outro = $_POST["organizacoes_outro"];
$participou_de_encontro = $_POST["participou_de_encontro"];
$condicoes_apos_filiacao = $_POST["condicoes_apos_filiacao"];
$condicoes_apos_filiacao_porque = $_POST["condicoes_apos_filiacao_porque"];
$aumento_de_rendimentos = $_POST["aumento_de_rendimentos"];
$vantagem_remuneracao = $_POST["vantagem_remuneracao"];
$vantagem_equipamentos_de_trabalho = $_POST["vantagem_equipamentos_de_trabalho"];
$vantagem_carga_horaria = $_POST["vantagem_carga_horaria"];
$vantagem_cursos = $_POST["vantagem_cursos"];
$vantagem_doacoes = $_POST["vantagem_doacoes"];
$vantagem_organizacao_da_comunidade = $_POST["vantagem_organizacao_da_comunidade"];
$vantagem_participacao_da_familia = $_POST["vantagem_participacao_da_familia"];
$vantagem_contato_com_instituicoes_de_apoio = $_POST["vantagem_contato_com_instituicoes_de_apoio"];
$preco_de_venda = $_POST["preco_de_venda"];
$catador_pais = $_POST["catador_pais"];
$catador_irmaos = $_POST["catador_irmaos"];
$catador_esposo = $_POST["catador_esposo"];
$catador_filhos = $_POST["catador_filhos"];
$catador_outro = $_POST["catador_outro"];
$condicao_familiar = $_POST["condicao_familiar"];
$falta_de_organizacao = $_POST["falta_de_organizacao"];
$distancias = $_POST["distancias"];
$inseguranca_financeira = $_POST["inseguranca_financeira"];
$baixa_remuneracao = $_POST["baixa_remuneracao"];
$relacionamento_catadores = $_POST["relacionamento_catadores"];
$relacionamento_comunidade = $_POST["relacionamento_comunidade"];
$falta_de_equipamentos = $_POST["falta_de_equipamentos"];
$lider_1 = $_POST["lider_1"];
$lider_2 = $_POST["lider_2"];

if(file_exists("init.php")) {
	require "init.php";		
} else {
	echo "Erro: Arquivo init.php nao foi encontrado.";
	exit;
}

if(!function_exists("Abre_Conexao")) {
	echo "Erro: O arquivo init.php foi alterado, nao existe a fun��o 'abre_conexao'";
	exit;
}

abre_conexao();
if(@mysql_query("INSERT INTO catadores VALUES (	NULL , '$data', '$grupo', '$nome', '$endereco', '$bairro', '$regiaometropolitana', '$telefone', '$datanascimento', '$sexo', '$raca', '$certidao_de_nascimento', '$certidao_de_casamento', '$rg', '$cpf', '$titulo_de_eleitor', '$carteira_de_trabalho', '$pis', '$nascido_em', '$cidade_natal', '$tempo_de_residencia', '$chefe_de_familia', '$filhos', '$estado_civil', '$residem_em_conjunto', '$menores_de_12', '$entre_12_e_18', '$que_trabalham', '$residencia', '$residencia_outro', '$situacao_da_residencia', '$situacao_da_residencia_outro', '$tipo_de_construcao', '$tipo_de_construcao_outro', '$contribuinte_do_inss', '$nao_contribui_ao_inss', '$nao_contribui_ao_inss_outro', '$renda_familiar', '$remuneracao', '$agua_encanada', '$energia_eletrica', '$esgoto', '$possui_banheiro', '$estudou_ate', '$supletivo', '$informatica', '$reciclagem', '$alfabetizacao', '$gestao', '$artesanato', '$estudar_outro', '$fuma', '$bebe', '$drogas', '$fumava', '$bebia', '$usava_drogas', '$frequenta_medico', '$faz_tratamento_de_saude', '$toma_medicamentos', '$faz_acompanhamento_psicologico', '$frequenta_dentista', '$atendimento_posto_de_saude', '$conhece_psf', '$ja_foi_atendido_psf', '$conhece_conselho_saude_local', '$participou_conselho_saude_local', '$religiao', '$trabalhou_antes', '$trabalhou_carteira_assinada', '$tempo_que_e_catador', '$sofreu_acidente', '$outro_emprego', '$pensao', '$aposentadoria', '$seguro_desemprego', '$auxilio_doenca', '$beneficio_de_prestacao_continuada', '$beneficio_outro', '$pensao_familia', '$aposentadoria_familia', '$seguro_desemprego_familia', '$auxilio_doenca_familia', '$beneficio_de_prestacao_continuada_familia', '$beneficio_familia_outro', '$peti', '$bolsa_familia', '$programa_do_leite', '$luz_fraterna', '$tarifa_social_da_agua', '$agente_jovem', '$programas_sociais_outro', '$doacoes', '$distancia', '$locomocao_ao_trabalho', '$locomocao_ao_trabalho_outro', '$cargo', '$mncr', '$instituto_lixo_cidadania', '$outras_associacoes', '$organizacoes_outro', '$participou_de_encontro', '$condicoes_apos_filiacao', '$condicoes_apos_filiacao_porque', '$aumento_de_rendimentos', '$vantagem_remuneracao', '$vantagem_equipamentos_de_trabalho', '$vantagem_carga_horaria', '$vantagem_cursos', '$vantagem_doacoes', '$vantagem_organizacao_da_comunidade', '$vantagem_participacao_da_familia', '$vantagem_contato_com_instituicoes_de_apoio', '$preco_de_venda', '$catador_pais', '$catador_irmaos', '$catador_esposo', '$catador_filhos', '$catador_outro', '$condicao_familiar', '$falta_de_organizacao', '$distancias', '$inseguranca_financeira', '$baixa_remuneracao', '$relacionamento_catadores', '$relacionamento_comunidade', '$falta_de_equipamentos', '$lider_1', '$lider_2' )")) {

	if(mysql_affected_rows() == 1){
		header( 'Location: index.php' ) ;
	}	

} else {
	if(mysql_errno() == 1062) {
		echo $erros[mysql_errno()];
		exit;
	} else {	
		echo "Erro: N�o foi possivel efetuar o cadastro.";
		exit;
	}	
	@mysql_close();
}

}
?>
