Algas:

Caracter�sticas das algas:
--------------------------
Foto-autotr�ficas; Muitas algas s�o m�veis ou possuem um est�gio m�vel durante o seu ciclo de vida.

S�o geralmente classificadas conforme as seguintes caracter�sticas: Natureza e propriedade de pigmentos. Natureza dos produtos de reserva e armazenamento. Tipo, n�mero, inser��o e morfologia de flagelos. Composi��o qu�mica e caracter�sticas f�sicas da parede celular. Morfologia e caracter�sticas das c�lulas e talos.

As algas microsc�picas e unicelulares � Euglenophyta, Chrysophyta e Pyrrophyta � s�o objeto de estudo da Microbiologia, assim como as formas unicelulares das Chlorophyta. As algas marrons (Phaeophyta) e as vermelhas (Rhodophyta) s�o estudadas na Bot�nica.

As algas possuem clorofila A como seu pigmento fotossint�tico prim�rio, assim como pigmentos caroten�ides acess�rios. Membros de Euglenophyta e Pyrrophyta n�o possuem parede celular. Outras algas possuem parede celular composta por s�lica, celulose, outros polissacar�deos ou �cidos org�nicos.

Euglenofitas (Euglenophyta)
---------------------------
Aut�trofos por fotoss�ntese. Presen�a de cloroplastos. Maioria de �gua doce. Componente importante do fitopl�ncton. Vac�olo puls�til para controle osm�tico. Estigma para percep��o de luz. Paramilo como reserva. Locomo��o por um flagelo. Reprodu��o assexuada por cissiparidade.

Euglen�fitas
------------
Dinoflagelados. Aut�trofos por fotoss�ntese. Maioria marinhas. Locomo��o por dois flagelos. Presen�a de bioluminesc�ncia. Reserva: amido e �leos. Reprodu��o assexuada por cissiparidade. Respons�veis pela mar� vermelha. Algas unicelulares coletivamente denominadas pl�ncton, ou organismos de livre flutua��o. Alguns produzem neurotoxinas.

Exemplos de Dinoflagelados: Gymnodinium, Ceratium, Noctiluca.

Cris�fitas (Chrysophyta)
------------------------
Algas douradas. Apresentam plast�deos verde amarelados, amarelos dourados e at� marrom. Aut�trofos por fotoss�ntese. Podem ocorrer mixotr�ficos (alimenta��o por osmose ou fagocitose). Ind�viduos unicelulares ou coloniales. Nanoplanct�nicas, mas podem existir spp. aderidas. Ambientes de oligo a mesotr�ficos.

Diatom�ceas (Bacillariophyta)
-----------------------------
Aut�trofos por fotoss�ntese. Sem c�lios ou flagelos. Parede celular impregnada de s�lica formando uma "carapa�a" (fr�stula). Gotas de �leo como reserva. Reprodu��o assexuada por cissiparidade ou sexuada por conjuga��o.

Bacilari�fitas
--------------
Registros f�sseis de 180 milh�es de anos. Diatomitos: dep�sitos de fr�stulas usados na fabrica��o de cosm�ticos, filtros, cer�micas, pastas de dente. De vida livre e perif�ticos. Dois grupos principais: diatom�ceas c�ntricas e diatom�ceas penadas. �timos indicadores ambientais.

Clorof�ceas (Chlorophyta)
-------------------------
Algas unicelulares e multicelulares (�nicas ou coloniais); Aut�trofos: clorofilas a e c; Amido como reserva de alimento; Com etapas biflageladas: flagelos do mesmo tamanho; Volvox, Chlamydomonas, Gonium.

Cianobact�rias (Cyanophyta)
---------------------------
Conhecidas vulgarmente como algas verde-azuladas. Como as bact�rias, s�o organismos procariontes cujas c�lulas n�o apresentam sistemas de membranas internas nas organelas citoplasm�ticas. Amplamente distribu�das no solo, �gua doce e em ambientes marinhos. Podem viver em associa��o (fixadoras de nitrog�nio).

Grande variedade de formas e arranjos, de cocos unicelulares a bacilos e mesmo filamentos longos e multicelulares. �nicas ou em col�nias (envoltas por mucilagem). Autotr�ficas (assimilam CO2) ou mixotr�ficas (assimilam compostos org�nicos). Vac�olos gasosos s�o a principal adapta��o � flutua��o. Apresentam um r�pido crescimento, formando flora��o ou "bloom". Organismos oportunistas ou r-estrategistas.

Fotopl�ncton:
Forma e tamanho: Muita varia��o (c�lulas, col�nias). Tamanho � a base para quantifica��o do biovolume. Categoriza��o de tamanho. Atividade biol�gica.

Pico e nanopl�ncton:
Foram negligenciadas no passado e assim seu papel tem sido subestimado. Estudos t�m mostrado sua grande import�ncia especialmente em ambientes oligotr�ficos (>90% da biomassa).

Significado biol�gico do tamanho e forma:
-----------------------------------------
As maiores diferen�as entre os fotossintetizantes procariontes e eucariontes tem grandes implica��es no tamanho (procariontes < 5�m; eucariontes devem ser maiores para conter as organelas). Ainda, muitas apresentam uma varia��o no formato da c�lula e da col�nia. Ex.:Alongada, com espinhos. etc.
Como conseq��ncia: Implica��es fisiol�gicas (trocas de material pela superf�cie, absor��o de luz, capacidade de crescimento r�pido). Distribui��o na coluna de �gua (movimento passivo, sedimenta��o, motilidade). Resist�ncia a ingest�o pelo zoopl�ncton	

Troca de material e a superf�cie da c�lula:
-------------------------------------------
� A difus�o passiva e troca de gases � um processo de m�o dupla; � Governada pelas Leis de Difus�o de Fick
(fluxo de mat�ria � proporcional � diminui��o da concentra��o (dc,) e inversamente proporcional � dist�ncia (dx) ) � Assim, a taxa de difus�o para a c�lula depender� da �rea superficial, do gradiente de concentra��o e do coeficiente de difus�o molecular.

Portanto: organismos com baixo S/V t�m problemas. j� que a �rea superficial � pequena em rela��o ao volume da c�lula e suas necessidades. � Organismos esf�ricos t�m as menores taxas poss�veis. Assim: S/V = (4�pi�r^2)/(4/3�pi�r^3) � Para uma c�lula esf�rica, um aumento no tamanho ir� reduzir a taxa S/V.

Tem alguma implica��o evolutiva? � Para manter uma elevada taxa S/V durante a evolu��o de organismos maiores, capazes de comportar organelas, surgiram as formas alongadas e a forma��o de col�nias.
Implica��es em rela��o � transfer�ncia de nutrientes: Clorof�ceas e cianof�ceas � os principais grupos: = menores e solit�rias encentram-se em ambientes oligotr�ficos = grandes e coloniais predominam em sistemas eutr�ficos.

Absor��o de luz e a superf�cie da c�lula:
-----------------------------------------
C�lulas menores apresentam uma maior efici�ncia na absor��o de luz por unidade de pigmento � Resultado: melhor efici�ncia fotossint�tica � Tamb�m apresentam melhor taxa de absor��o de UV e s�o assim mais suscept�veis � fotoinibi��o.

Tamanho como uma resposta ecol�gica:
------------------------------------
Algas pequenas e unicelulares t�m: # baixa biomassa # curto ciclo celular # r�pida taxa de crescimento � Portanto s�o adaptados a: # explorar ambientes com condi��es novas (as condi��es para o crescimento s�o limitadas no tempo) # ambientes sujeitos a stress. � Favorecem portanto organismos oportunistas ou r-estrategistas.

Condi��es de crescimento limitada pelo tempo:
---------------------------------------------
# Pico e nanopl�ncton tendem a dominar ambientes sub-polares # O per�odo de crescimento de ver�o � curto.
Condi��es de estresse ambiental: # ZPK representam o maior agente de stress ambiental # Contaminantes ambientais tamb�m t�m efeito dram�tico. � Como efeito, redu��o do tamanho dos organismos.

Sedimenta��o na coluna de �gua:
-------------------------------
Ocorre onde a densidade da part�cula excede a do meio (1000kg/m^3 para a �gua pura) � para o fitopl�ncton, o citoplasma `'e rico em macromol�culas as quais s�o mais densas que a �gua: - prote�nas: 1300kg/m3 - carboidratos: 1500kg/m3 - �cidos nucl�icos: 1700kg/m3 � por outro lado, a densidade da c�lula pode ser reduzida pela presen�a de l�quidos de baixa densidade e vac�olos gasosos.

Uma variedade de formas e col�nias t�m um grande efeito na taxa de sedimenta��o. � O desenvolvimento de formas atenuantes como col�nias alongadas e a presen�a de espinhos pode ser interpretado como uma adapta��o evolutiva para reduzir a sedimenta��o passiva destes organismos e manter a c�lula na zona f�tica. �alguns organismos, como as cianobact�rias, desenvolveram vac�olos para promover sua flutua��o.

Tamanho e motilidade
--------------------
O movimento das algas com flagelo � limitado pela for�a propulsiva deste � O movimento � de aproximadamente de 0.1-2m�/s. � A velocidade de nata��o deve exceder a de "sinking".

Resist�ncia � ingest�o pelo ZPK
--------------------------------
#Pode ser limitada pelo tamanho da c�lula #Formato da c�lula #Qu�mica superficial #Presen�a de mucilagem #Presen�a de toxinas #Algumas desenvolveram estrat�gias relacionadas ao tamanho: #Aumentando a dimens�o axial linear #Produzindo c�lulas �nicas #Produzindo col�nias

Habilidade competitiva geral:
-----------------------------
#O tamanho tamb�m � importante sob a �tica competitiva: #Estudos sugerem que o tamanho m�dio dos organismos aumentam com o aumento da biomassa (popula��o) #Assim: pequenas algas s�o melhores competidoras em comunidades esparsas e grandes algas mais eficientes em comunidades densas #G�neros com grande plasticidade fenot�pica tendem a serem bons competidores em ambos os ambientes #Turbul�ncia tamb�m tem efeitos sobre o tamanho das c�lulas e da col�nia: #Ambientes muito turbulentos s�o dominados por organismos menores e solit�rios.

Algas mucilaginosas e n�o mucilaginosas:
----------------------------------------
#As algas podem se apresentar tanto embebidas em mucilagem como livres dela, mas estudos recentes t�m demonstrado que todas possuem mucilagem, at� as diatom�ceas, cuja mucilagem reduz a possibilidade de dissolu��o da s�lica. #A mucilagem aumenta a dimens�o exterior e confere uma superf�cie quimicamente distinta � c�lula. #Composi��o: #Matriz polissacar�dica com macromol�culas; #�gua � > 95% #Dominada por a�ucares e grupos carregados associados e eles.

Papel da mucilagem:
-------------------
#"Sinking rate": Depende do aumento do tamanho e da redu��o na densidade. #"grazing" e digest�o pelo ZPK: #A mucilagem � um importante fator de sele��o pelo KPK, tanto pelo tamanho da c�lula quanto pela composi��o qu�mica da superf�cie #A mucilagem pode prevenir a digest�o da alga, sendo devolvida ao meio ainda vi�vel. #Associa��o da biota epif�tica: mucilagem prov� substrato para ades�o e adsor��o de organismos, mat�ria org�nica e nutrientes (formando uma microsfera ou micro-ecossistema)

#Adsor��o de �nions e c�tions: #conferindo uma facilita��o na absor��o de nutrientes #habilidade � c�lula algal resistir a metais pesados.

Fitopl�ncton e grau de trofia Rela��o C:N:P (106C : 16N : 1P)

Fitopl�ncton: varia��o temporal
-------------------------------
#A habilidade de responder a mudan�as do ambiente, � claramente importante para o sucesso biol�gicos das algas. #A atividade das algas pode variar temporalmente entre fra��es de secundo e centenas de anos: #Exclui: altera��es celulares de curto tempo sucess�o em tempo m�dio altera��es de longa dura��o nos ecossistemas

FPK: sucess�o ecol�gica
-----------------------
#Etapas: # Mudan�a induzida pelo ambiente: domina o primeiro est�gio sucessional; # Competi��o de esp�cies: come�a quando organismos dependem do mesmo recurso de consumo e quando est� limitado; o consumo por uma spp limita a outra assim como seu crescimento. #Maiores recursos: luz, espa�o e nutrientes; # Antagonismo entre esp�cies: a inibi��o da outra esp�cie � feita por um composto antimicrobiano como um antibi�tico; # Adapta��o n�o competitiva: seu sucesso se deve mais � sua capacidade de adapta��o �s condi��es ambientais dos que da intera��o direta com outras esp�cies.

Biofilme e comunidade perif�tica � Est�gios:
--------------------------------------------
#Coloniza��o inicial da superf�cie exposta #Desenvolvimento de um biofilme de Diatom�ceas #Coloniza��o por algas filamentosas #Forma��o de uma comunidade perif�tica madura #Perda de fragmentos devido � corrente e ondas e rein�cio da sucess�o aos est�gios iniciais.

Distribui��o na coluna de �gua:
#Altamente din�mica #Relacionada aos par�metros ambientais #Apresenta varia��es di�rias e sazonais

Migra��o ativa das algas:
#Utilizando motilidade ativa (flagelo) #Produzindo vac�olos gasosos # Outras, como as bacilariof�ceas, apenas podem reduzir sua taxa de sedimenta��o.

Fatores afetando a ocorr�ncia e distribui��o do FPK:
#Migra��o ativa #Movimento passivo #N�veis de popula��es pr�existentes

Medidas de diversidade e biomassa
#Diversidade: T�cnica Iterm�hl

#Biomassa:
Ln(C) = -1.952 + 0.996 Ln(V)
Clorofila-a
