Artigo 13.4: Controle da Qualidade Microbiol�gica das �guas de Consumo na Microbacia Hidrogr�fica Arroio Passo do Pil�o


Antecedentes e caracteriza��o do problema
A �gua � um recurso natural essencial � vida e ao desenvolvimento das comunidades humanas. E, ainda que, considerada uma reserva mineral barata e inesgot�vel � de direito de todos, independente do est�gio de desenvolvimento ou condi��o s�cio-econ�mica devendo atender a todas as necessidades humanas fisiol�gicas, econ�micas e dom�sticas com quantidade, continuidade, cobertura e custo. Entretanto, n�o basta que as popula��es apenas disponham de �gua, � necess�rio tamb�m que essa �gua se caracterize por um m�nimo de qualidade. As preocupa��es quanto aos n�veis de qualidade, contamina��o das �guas e manuten��o dos recursos h�dricos assumem import�ncia, � medida que a �gua � destinada ao consumo humano ou a transforma��o econ�mica. �gua n�o pot�vel, ou seja, contaminada de alguma forma por agentes patog�nicos nocivos pode por em perigo a sa�de e comprometer o desenvolvimento das comunidades humanas. A �gua comp�e um importante meio de transmiss�o de doen�as. Fatos hist�ricos demonstram que algumas das mais generalizadas epidemias que j� infligiram as popula��es humanas, com exce��o da peste bub�nica, tiveram sua origem em sistemas de distribui��o de �gua (BRANCO, 1999).

1

2

Eng. Agr., Embrapa Clima Temperado, Cx. Postal 403, 96001-970, Pelotas, RS e-mail: mattos@cpact.embrapa.br Ec�logo, P�s-graduando, UFPel/FAEM, Cx. Postal, 354, CEP 96001-970, Pelotas, RS

2

Controle da Qualidade Microbiol�gica das �guas de Consumo na Microbacia Hidrogr�fica Arroio Passo do Pil�o

Escherichia coli � o organismo mais estudado em todo o mundo, sendo a bact�ria mais isolada em laborat�rios cl�nicos de microbiologia. Al�m de ser o primeiro organismo na lista das infec��es urin�rias Escherichia coli tem sido, tamb�m, isolada de outros diversos s�tios do corpo humano, respons�vel por patologias como pneumonias, meningites e infec��es intestinais. Algumas cepas patog�nicas de Escherichia coli, com endotoxinas potentes podem causar diarr�ias moderadas a severas, colite hemorr�gica grave, e a s�ndrome hemol�tica ur�mica (SHU) em todos os grupos et�rios, levando o indiv�duo � morte, quando o tratamento n�o � eficaz (ZIESE et al., 1996). O efeito de uma cepa � fun��o do sorotipo envolvido. O sorotipo mais grave � o entero-hemorr�gico (EHEC) que ocorre unicamente como (O157:H7), cepa respons�vel por 15% das complica��es que levam a SHU em idosos e crian�as menores de cinco anos. A SHU caracteriza-se pela destrui��o das c�lulas vermelhas do sangue e fal�ncia renal que pode ser acompanhada de deteriora��o neurol�gica e insufici�ncia renal cr�nica (ZIESE et al., 1996; SILVA, 1999; KONEMAN et al., 2001). A an�lise bacteriol�gica surge como uma importante ferramenta ao reconhecimento da qualidade da �gua de consumo. T�cnicas bacteriol�gicas s�o sens�veis e espec�ficas ao agente patog�nico investigado em qualquer inst�ncia, seja no alimento, no solo ou na �gua. Estudos de qualidade de �gua, recentemente desenvolvidos na microbacia hidrogr�fica arroio Passo do Pil�o, sugerem que a altera��o da qualidade microbiol�gica das �guas de consumo n�o � resultante somente de fatores naturas como tamb�m de uso e ocupa��o deste ambiente. A pouca informa��o, a falta de estrutura sanit�ria, a m� conserva��o dos po�os dom�sticos de abastecimento, a falta de manuten��o dos reservat�rios, a baixa qualidade das redes de distribui��o e, principalmente, o manejo inadequado de deje��es animais, incorporadas ao solo sem tratamento, s�o, talvez, os fatores humanos mais importantes (SILVA & MATTOS, 2001). Regi�o de car�ter agr�cola e pecu�rio, t�m sua economia rural voltada ao desenvolvimento agr�cola de pequena propriedade. Re�ne 98 estabelecimentos rurais, por�m nem todos de status produtivo, alguns apenas s�tios de lazer ocupados durante os finais de semana. Nesta microbacia foram investigados dez estabelecimentos rurais produtivos que obtinham sua �gua de consumo de po�os dom�sticos. Os resultados de an�lises para verifica��o da qualidade microbiol�gica da �gua de consumo, realizadas nos per�odos de chuva (dezembro a mar�o) e seca (abril a setembro), demonstraram que 100% dos pontos investigados apresentaram a presen�a de microrganismos coliformes, inclusive coliforme fecal, indicando que uma via de contamina��o fecal est� aberta e que outros organismos, tamb�m patog�nicos, podem estar presentes.

Entretanto, embora a an�lise qualitativa tenha revelado contamina��o significativa, tanto para coliformes fecais quanto por coliformes totais, a an�lise quantitativa, n�o expressou contamina��o fecal, apenas contamina��o por coliformes totais de cepas n�o identificadas e outras Enterobact�rias de cepas tamb�m n�o identificadas (SILVA & MATTOS, 2001). Por�m, nas condi��es estabelecidas pela resolu��o n� 20, de 18 de junho de 1986, do Conselho Nacional do Meio Ambiente (CONAMA), para �guas destinadas ao abastecimento dom�stico sem pr�via desinfec��o, deve haver aus�ncia de coliformes totais. O fato de a contamina��o por coliformes fecais n�o se expressar na an�lise quantitativa n�o significa aus�ncia de contamina��o. Apenas os valores de contamina��o n�o atingiram a sensibilidade do m�todo e, portanto, uma via de cont�gio permanece aberta e a��es sanit�rias se fazem necess�rias.

Descri��o da Recomenda��o
Para que o controle microbiol�gico das �guas de consumo se efetive � necess�rio que medidas e a��es sejam adotadas. O tratamento de dejetos animais anterior a sua incorpora��o ao solo, o saneamento b�sico e a manuten��o do sistema de armazenamento e distribui��o de �gua domiciliar, constituem o primeiro passo. Nos domic�lios a �gua destinada ao consumo deve ser proveniente de po�os dom�sticos bem preservados e manutenidos uma vez a cada ano. O sistema de armazenamento e distribui��o deve estar conservado e livre de vazamentos. Caixas d'�gua devem ser esvaziadas e limpas a cada seis meses. As torneiras devem estar em boas condi��es de uso e de prefer�ncia serem met�licas. Torneiras pl�sticas sofrem agress�o interna e acumulo de materiais formando filmes microbianos, observados, geralmente, nas bordas. O uso de filtro � recomendado e, na falta deste, a �gua deve ser fervida por alguns minutos. Outra t�cnica para a preven��o de agentes pat�genos na �gua de consumo � a clora��o do po�o. Entretanto, esta � uma pr�tica que merece aten��o especial, por se tratar da adi��o de um qu�mico na �gua, o cloro, devendo ser acompanhada por um especialista (SOARES & MAIA, 1999). A manuten��o da qualidade de �gua garante a sa�de e o desenvolvimento das comunidades humanas. �gua sem qualidade, ou seja, contaminada por algum agente patog�nico pode conduzir a preju�zos na sa�de ou mesmo levar a outros efeitos negativo.

Agradecimentos
� Funda��o Coordena��o de Aperfei�oamento de Pessoal de N�vel Superior (CAPES), pela concess�o da bolsa de estudos.

Controle da Qualidade Microbiol�gica das �guas de Consumo na Microbacia Hidrogr�fica Arroio Passo do Pil�o

3

Refer�ncias Bibliogr�ficas
BRANCO, S.M. �gua, Meio Ambiente e Sa�de. �guas Doces no Brasil. S�o Paulo: Escrituras Editora, 1999 p. 227, 248. HOFSTRA, H; HUISIN'T VELD, J.H.J. Methods for the detection and isolation of Escherichia coli including pathogenic strains. Journal of Applied Bacteriology Symposium Supplement, p.197-212. 1988. KONEMAN, E.W.; ALLEN, S.D.; JANDA, W.M.; SCHRECKENBERGER, P.C.; WINN Jr., W.C. Diagn�stico Microbiol�gico. 5.ed., Rio de Janeiro: MEDSI, 2001. 1465p. SILVA, C.H.P.M. Bacteriologia: Um texto ilustrado. Minas Gerais: PUC, PUC Eventos, 1999. 531p.

SILVA, M.D. da; MATTOS, M.L.T. Microbiological quality of water for human consumption in the hydrographical microbasin of arroio Passo do Pil�o. In: CONGRESSO BRASILEIRO DE MICROBIOLOGIA, 21, 2001, Foz do Igua�u. Resumos... Foz do Igua�u, 2001. p. 42. SOARES, J.B.; MAIA, A.C.F. �gua: microbiologia e tratamento. Fortaleza: UFC, UFC edi��es, 1999. 206p. SOUZA, L.C.; IARIA, S.T.; LOPES, C.A.M. Bact�rias coliformes totais e coliformes de origem fecal em �guas usadas na dessedenta��o de animais. Revista Sa�de P�blica, S�o Paulo, v.17, n.2, p.112-122, 1983. SZEWZYK, U.; SZEWZYK, R.; MANZ, W.; SCHLEIFER, K.H. Microbiological safety of drinking water. Annu. Rev. Microbiol., 2000, n.54, p.81-127. ZIESE, T.; ANDERSON, Y.; DE JONG, B.; L�FDAHL, S.; RAMBERG, M. Surto de Escherichia coli O157 na Su�cia. Relat�rios de investiga��o de surtos. Vol.1, n.1, 1996. 16p.

*M�dias seguidas por letras min�sculas distintas, na mesma linha, e mai�sculas, na mesma coluna, diferem entre si pelo teste de Duncan a 5%.

Comunicado Exemplares desta edi��o podem na mesma linha, *M�dias seguidas por letras min�sculas distintas,ser adquiridos na: e mai�sculas, na mesma coluna, diferem entre si pelo teste de Publica��es Secret�ria-Executiva: Joseane M. Lopes Garcia T�cnico, 61 Embrapa Clima Temperado Duncan a 5%.
MINIST�RIO DA AGRICULTURA, PECU�RIA E ABASTECIMENTO

Comit� de

Presidente: M�rio Franklin da Cunha Gastal

Endere�o: Caixa Postal 403 Fone: (53) 275 8199 Fax: (53) 275 8219 - 275 8221 E-mail: sac@cpact.embrapa.br 1� edi��o 1� impress�o (2002): 50

Membros: Ariano Martins Magalh�es Junior, Fl�vio Luiz Carpena Carvalho, Darcy Bitencourt, Cl�udio Jos� da Silva Freire, Vera Allgayer Os�rio, Suplentes: Carlos Alberto Barbosa Medeiros e Eva Choer

Expediente

Supervisor editorial: Maria Devanir Freitas Rodrigues Revis�o de texto: Maria Devanir Freitas Rodrigues/Ana Luiza Barragana Viegas Editora��o eletr�nica: Oscar Castro


