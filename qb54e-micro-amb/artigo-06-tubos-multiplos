Artigo 13.6: Uso do Método dos Tubos Múltiplos Para Quantificação de Coliformes Totais e Termotolerantes

Para avaliação da qualidade da água do ponto de vista bacteriológico é imprescindível a utilização de organismos indicadores de contaminação fecal, as bactérias do grupo das coliformes são as principais indicadoras de contaminação fecal, sendo empregadas como parâmetro bacteriológico básico na definição de padrões para o monitoramento da qualidade da água.

Os coliformes totais são definidos como bacilos gram negativos, aeróbios ou anaeróbios facultativos, não formadores de esporos, que fermentam lactose a 35-37°C com produção de ácido, gás e aldeído em até 48h. Já os termotolerantes se diferem pelo teste da lactose, mas a 44,5°C em 24h.

Dentre as metodologias mais comumente utilizadas para determinação destes organismos em ambientes aquáticos está o método dos tubos múltiplos, que estima a densidade das bactérias através da combinação dos resultados positivos e negativos dos tubos, e é expresso como número mais provável (NMP/100mL). Este número é obtido através de tabelas estatísticas específicas denominadas tabelas do Número Mais Provável (NMP).

A coleta do ambiente escolhido deve ser representativa, e o recipiente deve ser estéril - assim como todo material utilizado - com cuidados especiais para evitar contaminações.

Normalmente a série de tubos usadas são de 10mL de amostra na primeira fileira, depois para a segunda fileira 1mL, e na terceira 0,1mL retirados diretamente da amostra. A partir daí deve-se diluir a amostra utilizando-se tubos de diluição.

Com todos os procedimentos de inoculação através da chama concluídos, os tubos devem ser agitados e acondicionados em estufa a 36°C por 48 a 72h. A leitura é realizada após 48h, sendo considerados positivos com a presença de coliformes quando o meio está turvo e apresenta bolhas de gás no tubo Durham. Havendo turvação sem bolhas, o tubo deve voltar à estufa por mais 24h para mais uma leitura. Apenas a turvação não confere positividade.

Dos tubos positivos será retirado apenas um inóculo com alça de níquel cromo ou de platina para o tubo EC. Os tubos EC deverão ser incubados em banho maria a 44°C por 24h, do mesmo modo que para os coliformes totais. Após isso efetua-se a leitura.

Obtida a contaminação de tubos positivos e negativos, tanto de coliformes termotolerantes  quanto totais, comparam-se os valores na tabela de modo a transformar o resultado em NMP/100mL, quantificando-se assim as bactérias do grupo dos coliformes de cada amostra.
